<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/



// rotas site

Route::get('/', function()
{
    return View::make('ecliques.index');
});


Route::resource('novousuario', 'UserController');

Route::resource('publicar', 'PublicarController');
Route::resource('publicar/forms', 'PublicarController@forms');

Route::get('{city?}/{category}/{id?}', function( $city = null, $category = null, $id = null )
{
    $product = new ProductsController;

    if( is_null($id))
        return $product->productsRegions( [ 'city' => $city, 'category' => $category ] );
    
    return $product->productsRegionsFindId( [ 'city' => $city, 'category' => $category, 'id' => $id ] );
       
})
->where( array( 'city' => '[a-z]+', 'category' => '[a-z]+', 'id' => '[0-9]+' ));


Route::post('pesquisa', 'ProductsController@pesquisa');

//brasil exibe todos produtos
Route::resource('brasil', 'ProductsController');

// estados
Route::resource('brasil', 'ProductsController');
Route::resource('amazonas', 'ProductsController');
Route::resource('acre', 'ProductsController');
Route::resource('alagoas', 'ProductsController');
Route::resource('amapa', 'ProductsController');
Route::resource('ceara', 'ProductsController');
Route::resource('distritofederal', 'ProductsController');
Route::resource('espiritosanto', 'ProductsController');
Route::resource('maranhao', 'ProductsController');
Route::resource('parana', 'ProductsController');
Route::resource('pernambuco', 'ProductsController');
Route::resource('parana', 'ProductsController');
Route::resource('pernambuco', 'ProductsController');
Route::resource('piaui', 'ProductsController');
Route::resource('riograndedonorte', 'ProductsController');
Route::resource('riograndedonsul', 'ProductsController');
Route::resource('rondonia', 'ProductsController');
Route::resource('roraima', 'ProductsController');
Route::resource('santacatarina', 'ProductsController');
Route::resource('sergipe', 'ProductsController');
Route::resource('tocantins', 'ProductsController');
Route::resource('para', 'ProductsController');
Route::resource('bahia', 'ProductsController');
Route::resource('goias', 'ProductsController');
Route::resource('matogrosso', 'ProductsController');
Route::resource('matogrossodosul', 'ProductsController');
Route::resource('riodejaneiro', 'ProductsController');
Route::resource('saopaulo', 'ProductsController');
Route::resource('riograndedosul', 'ProductsController');
Route::resource('minasgerais', 'ProductsController');
Route::resource('paraiba', 'ProductsController');


Route::post('post/setpostsbuyer', 'PostController@setpostsbuyer');

// rotas admin

Route::get('logout',  array('as' => 'admin.logout',      'uses' => 'App\Controllers\Admin\AuthController@getLogout'));
Route::get('login',   array('as' => 'admin.login',       'uses' => 'App\Controllers\Admin\AuthController@getLogin'));
Route::post('login',  array('as' => 'admin.login.post',  'uses' => 'App\Controllers\Admin\AuthController@postLogin'));

Route::group(array('before' => 'auth.admin'), function()
{
	Route::any('/',                'App\Controllers\Admin\PagesController@index');
	Route::resource('private',       'App\Controllers\Admin\PrivateController');
        
        // partials
	   Route::resource('destacar', 'App\Controllers\Admin\ProdutosController@destacar');
       Route::resource('desativar', 'App\Controllers\Admin\ProdutosController@desativar');
       Route::resource('contratar', 'App\Controllers\Admin\ProdutosController@contratar');
        Route::post('produtos/atualiza', 'App\Controllers\Admin\ProdutosController@atualiza');

        Route::resource('produtos',     'App\Controllers\Admin\ProdutosController');
        Route::resource('notificacoes', 'App\Controllers\Admin\PartialsController@notificacoes');
        Route::resource('mensagens',     'App\Controllers\Admin\MensagemController');

        Route::resource('todasmensagens',     'App\Controllers\Admin\MensagemController@todasMensagens');

        Route::resource('post/ativar', 'PostController@ativar');
        Route::resource('post/responder', 'PostController@responder');
        Route::resource('post/remover', 'PostController@remover');
});