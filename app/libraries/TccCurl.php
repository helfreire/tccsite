<?php

class TccCurl extends Curl{

	protected $url = 'http://tccwebservice.hf/api/v1/';
	private $login 	= 'tcc1';
	private $pass	= 'tcc';

	function __construct($action = ''){
		parent::__construct($this->url . $action);
	}

	public function request(){
		parent::http_login( 'tcc1', 'tcc' );
		parent::execute();
		return self::response();
	}

	private function response(){
		
		switch ( $this->info['http_code'] ) {
            case 200:
                return json_decode($this->last_response);
                break;
            case 500:
            	return '500 Internal Server Error'; 
            	break;
            default:
                return $this->debug();
                break;
        }
	}

}        