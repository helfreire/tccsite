<div class="col-md-4">
    <div class="content-box mobile-buttons  text-center pad20A" style="border: hidden;">
        <div class="row pad10A">
        {{ Form::open(array('url' => 'pesquisa')) }}
        <div class="form-row">
            <div class="form-input col-md-12">
                <div class="form-input-icon">
                    <i class="glyph-icon icon-search gradient-green"></i>
                     {{ Form::text( 'pesquisa' ,'', array( 'placeholder' => 'Pesquise seus produtos aqui' )) }}
                </div>
            </div>
        </div>
        {{ Form::input('hidden', 'route', $route ) }}
        {{ Form::submit('Iniciar pesquisa', array( 'class' => 'pad10A btn primary-bg float-left') ) }}
        {{ Form::close() }}
    </div>
        <div class="row pad10A">
            <h3 class="mrg0A">Categorias</h3>
        </div>
        <div class="divider"></div>
        <div class="row pad10A">
            <div class="col-md-4">
                <a style="width:80px; height:85px;" href="{{ URL::to( $route  . '/animais' ) }}" class="btn vertical-button solid-white" title="">
                    <span class="glyph-icon icon-separator-vertical gradient-blue pad0A radius-all-4 mrg5T mrg10B large">
                        <i class="glyph-icon icon-credit-card font-size-20 opacity-60"></i>
                    </span>
                    <span class="button-content font-gray-dark font-bold text-transform-upr font-size-12">Animais</span>
                </a>
            </div>
            <div class="col-md-4">
                <a style="width:80px; height:85px;" href="{{ URL::to( $route  . '/bebesecriancas' ) }}" class="btn vertical-button solid-white" title="">
                    <span class="glyph-icon icon-separator-vertical gradient-blue pad0A radius-all-4 mrg5T mrg10B large">
                        <i class="glyph-icon icon-beaker font-size-20 opacity-60"></i>
                    </span>
                    <span class="button-content font-gray-dark font-bold text-transform-upr font-size-12">Bebês e crianças</span>
                </a>
            </div>
            <div class="col-md-4">
                <a  href="{{ URL::to( $route  . '/eletronicostelefonia' ) }}" class="btn vertical-button solid-white" title="">
                    <span class="glyph-icon icon-separator-vertical gradient-blue pad0A radius-all-4 mrg5T mrg10B large">
                        <i class="glyph-icon icon-camera font-size-20 opacity-60"></i>
                    </span>
                    <span class="button-content font-gray-dark font-bold text-transform-upr font-size-12">Eletrônicos e telefonia</span>
                </a>
            </div>
        </div>
        <div class="row pad10A">
            <div class="col-md-4">
                <a style="width:80px; height:85px;" href="{{ URL::to( $route  . '/empregosenegocios' ) }}" class="btn vertical-button solid-white" title="">
                    <span class="glyph-icon icon-separator-vertical gradient-blue pad0A radius-all-4 mrg5T mrg10B large">
                        <i class="glyph-icon icon-briefcase font-size-20 opacity-60"></i>
                    </span>
                    <span class="button-content font-gray-dark font-bold text-transform-upr font-size-12">Empregos e negócios</span>
                </a>
            </div>
            <div class="col-md-4">
                <a style="width:80px; height:85px;" href="{{ URL::to( $route  . '/esportes' ) }}" class="btn vertical-button solid-white" title="">
                    <span class="glyph-icon icon-separator-vertical gradient-blue pad0A radius-all-4 mrg5T mrg10B large">
                        <i class="glyph-icon icon-beaker font-size-20 opacity-60"></i>
                    </span>
                    <span class="button-content font-gray-dark font-bold text-transform-upr font-size-12">Esportes</span>
                </a>
            </div>
            <div class="col-md-4">
                <a style="width:85px; height:85px;" href="{{ URL::to( $route  . '/hobbiesmusica' ) }}" class="btn vertical-button solid-white" title="">
                    <span class="glyph-icon icon-separator-vertical gradient-blue pad0A radius-all-4 mrg5T mrg10B large">
                        <i class="glyph-icon icon-download-alt font-size-20 opacity-60"></i>
                    </span>
                    <span class="button-content font-gray-dark font-bold text-transform-upr font-size-12">Hobbies e música</span>
                </a>
            </div>
        </div>
        <div class="row pad10A">
            <div class="col-md-4">
                <a style="width:80px; height:85px;" href="{{ URL::to( $route  . '/imoveis' ) }}" class="btn vertical-button solid-white" title="">
                    <span class="glyph-icon icon-separator-vertical gradient-blue pad0A radius-all-4 mrg5T mrg10B large">
                        <i class="glyph-icon icon-hospital font-size-20 opacity-60"></i>
                    </span>
                    <span class="button-content font-gray-dark font-bold text-transform-upr font-size-12">Imóveis</span>
                </a>
            </div>
            <div class="col-md-4">
                <a style="width:80px; height:85px;" href="{{ URL::to( $route  . '/veiculosebarcos' ) }}" class="btn vertical-button solid-white" title="">
                    <span class="glyph-icon icon-separator-vertical gradient-blue pad0A radius-all-4 mrg5T mrg10B large">
                        <i class="glyph-icon icon-beaker font-size-20 opacity-60"></i>
                    </span>
                    <span class="button-content font-gray-dark font-bold text-transform-upr font-size-12">Veiculos e barcos</span>
                </a>
            </div>
            <div class="col-md-4">
                <a  style="width:85px; height:85px;" href="{{ URL::to( $route  . '/modaebeleza' ) }}" class="btn vertical-button solid-white" title="">
                    <span class="glyph-icon icon-separator-vertical gradient-blue pad0A radius-all-4 mrg5T mrg10B large">
                        <i class="glyph-icon icon-download-alt font-size-20 opacity-60"></i>
                    </span>
                    <span class="button-content font-gray-dark font-bold text-transform-upr font-size-12">Moda e beleza</span>
                </a>
            </div>
        </div>
        <div class="row pad10A">
            <div class="col-md-4">
                <a style="width:80px; height:85px;" href="{{ URL::to( $route  . '/parasuacasa' ) }}" class="btn vertical-button solid-white" title="">
                    <span class="glyph-icon icon-separator-vertical gradient-blue pad0A radius-all-4 mrg5T mrg10B large">
                        <i class="glyph-icon icon-home font-size-20 opacity-60"></i>
                    </span>
                    <span class="button-content font-gray-dark font-bold text-transform-upr font-size-12">Para sua casa</span>
                </a>
            </div>
        </div>
    </div>
</div>