<!doctype html>
<html lang="pt">
    <head>
        <meta charset="utf-8">
        <title>Admin</title>
        @include('admin.partials._assets')
        {{ HTML::script('assets/chosen/chosen.jquery.js') }}
        @yield('script')
    </head>
    <body>
        <div id="page-wrapper">
            @include ( 'products._product_navbar' )
            <div id="page-content">
                <div class="row">
                    <div  >
                       @include ( 'layouts._menu_lateral' )
                    </div>
                    <div class="col-md-8">

                        @yield('main')        

                    </div>
                </div>
            </div>
        </div>
    </body>
</html>