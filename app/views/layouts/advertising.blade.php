<!doctype html>
<html lang="en">
<head>

	{{ HTML::style('assets/bootstrap/css/bootstrap.min.css') }}
	{{ HTML::style('assets/bootstrap/css/bootstrap-responsive.css') }}
	{{ HTML::script('assets/bootstrap/js/bootstrap.js') }}
    {{ HTML::script('assets/js/jquery-1.8.3.min.js') }}
    {{ HTML::script('assets/js/jquery.livequery.js') }}

    @yield('script')
    @yield('css')

    <meta charset="UTF-8">
    <title>Cadastrar anúncio grátis</title>
</head>
<body>
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span16">
                @yield('menutop')
            </div>
        </div>
    	<div class="row-fluid">
    		<div class="span9 offset1">
    			@yield('content')
    		</div>
    		<div class="span2">
    			
    		</div>
    	</div>
    	<div class="row-fluid">
    		<div class="span16">
    		
    		</div>
    	</div>
    </div>
</body>
</html>