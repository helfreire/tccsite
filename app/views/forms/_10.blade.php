 <!--- Empregos e negócios - Serviços -->

<div class="control-group">
  <label class="control-label" for="selEmpNeSer">Tipo</label>
  <div class="controls">
    <select id="selEmpNeSer">       
      <option value="">Selecione o tipo</option>
      <option value="Babá" >Babá</option>
      <option value="Comunidade - Voluntários" >Comunidade - Voluntários</option>
      <option value="Consertos - Reformas - Pedreiros" >Consertos - Reformas - Pedreiros</option>
      <option value="Cursos - Aulas" >Cursos - Aulas</option>
      <option value="Eventos - Festas" >Eventos - Festas</option>
      <option value="Limpeza - Faxina - Diarista" >Limpeza - Faxina - Diarista</option>
      <option value="Profissionais liberais" >Profissionais liberais</option>
      <option value="Saúde - Beleza - Fitness" >Saúde - Beleza - Fitness</option>
      <option value="Serviços de informática" >Serviços de informática</option>
      <option value="Transporte - Mudanças" >Transporte - Mudanças</option>
      <option value="other" >Outros</option>
    </select>
  </div>
</div>