 <!--- animais - outros animais -->

<div class="control-group">
  <label class="control-label" for="selAniOutros">Tipo de animal</label>
  <div class="controls">
    <select id="selAniOutros">       
      <option value="">Selecione o tipo</option>
      <option value="Aves" >Aves</option>
      <option value="Cavalos" >Cavalos</option>
      <option value="Gatos" >Gatos</option>
      <option value="Peixes" >Peixes</option>
      <option value="Roedores" >Roedores</option>
      <option value="other" >Outros</option>
    </select>
  </div>
</div>