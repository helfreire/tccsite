 <!--- Eletrônicos e telefonia - Computadores e Periféricos -->

<div class="control-group">
  <label class="control-label" for="selEleTelCompPeri">Tipo</label>
  <div class="controls">
    <select id="selEleTelCompPeri">       
      <option value="">Selecione o tipo</option>
      <option value="Componentes - Peças" >Componentes - Peças</option>
      <option value="Computadores PCs" >Computadores PCs</option>
      <option value="Impressoras e Acessórios" >Impressoras e Acessórios</option>
      <option value="iPad - Tablets" >iPad - Tablets</option>
      <option value="Monitores" >Monitores</option>
      <option value="Notebooks" >Notebooks</option>
      <option value="Periféricos - Acessórios" >Periféricos - Acessórios</option>
      <option value="Redes Wi-Fi" >Redes Wi-Fi</option>
      <option value="other" >Outros</option>
    </select>
  </div>
</div>