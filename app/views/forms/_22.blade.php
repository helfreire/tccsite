 <!--- Imóveis - Vendas - Casas e Apartamento -->

<div class="control-group">
  <label class="control-label" for="selEmpNeSer">Quartos</label>
  <div class="controls">
    <select id="selEmpNeSer">       
      <option value="">Selecione a quantidade</option>
      <option value="Estúdio" >Estúdio</option>
      <option value="1" >1</option>
      <option value="2" >2</option>
      <option value="3" >3</option>
      <option value="4" >4</option>
      <option value="5" >5</option>
      <option value="6" >6</option>
      <option value="7" >7</option>
      <option value="8" >8</option>
      <option value="9" >9</option>
      <option value="10" >10</option>
    </select>
  </div>
</div>
<div class="control-group">
  <label class="control-label" for="area">Área construída</label>
  <div class="controls">
    <input id="area" name="area" type="text"> m²
  </div>
</div>
<div class="control-group">
  <label class="control-label" for="vagas">Vagas na garagem</label>
  <div class="controls">
    <input id="vagas" name="vagas" type="text">
  </div>
</div>
<div class="control-group">
  <label class="control-label" for="condominio">Condomínio (R$)</label>
  <div class="controls">
    <input id="condominio" name="condominio" type="text">,00
  </div>
</div>
<div class="control-group">
  <label class="control-label" for="iptu">IPTU (R$)</label>
  <div class="controls">
    <input id="iptu" name="iptu" type="text">,00
  </div>
</div>