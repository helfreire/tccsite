 <!--- animais - cachorros -->

<div class="control-group">
  <label class="control-label" for="selAniraca">Raça</label>
     <div class="controls">
        <select id="selAniraca">       
            <option value="">Selecione a raça</option>
            <option value="Akita" >Akita</option>
            <option value="Basset Hounds" >Basset Hounds</option>
            <option value="Beagle" >Beagle</option>
            <option value="Bernese Mountain" >Bernese Mountain</option>
            <option value="Border Collie" >Border Collie</option>
            <option value="Boxer" >Boxer</option>
            <option value="Bulldog" >Bulldog</option>
            <option value="Bull Terrier" >Bull Terrier</option>
            <option value="Chihuahua" >Chihuahua</option>
            <option value="Chow Chow" >Chow Chow</option>
            <option value="Cocker" >Cocker</option>
            <option value="Dálmata" >Dálmata</option>
            <option value="Doberman" >Doberman</option>
            <option value="Fila Brasileiro" >Fila Brasileiro</option>
            <option value="Fox Paulistinha" >Fox Paulistinha</option>
            <option value="Golden Retriever" >Golden Retriever</option>
            <option value="Husky Siberiano" >Husky Siberiano</option>
            <option value="Labrador" >Labrador</option>
            <option value="Lhasa Apso" >Lhasa Apso</option>
            <option value="Maltês" >Maltês</option>
            <option value="Pastor Alemão" >Pastor Alemão</option>
            <option value="Pequinês" >Pequinês</option>
            <option value="Pinscher" >Pinscher</option>
            <option value="Pit Bull" >Pit Bull</option>
            <option value="Poodle" >Poodle</option>
            <option value="Pug" >Pug</option>
            <option value="Rottweiler" >Rottweiler</option>
            <option value="Schnauzer" >Schnauzer</option>
            <option value="Shar Pei" >Shar Pei</option>
            <option value="Shih Tzu" >Shih Tzu</option>
            <option value="Spitz Alemão" >Spitz Alemão</option>
            <option value="Yorkshire" >Yorkshire</option>
            <option value="other" >Outros</option>
        </select>
    </div>
</div>