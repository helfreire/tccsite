 <!--- Imóveis - Temporada -->

<div class="control-group">
  <label class="control-label" for="selEmpNeSer">Quartos</label>
  <div class="controls">
    <select id="selEmpNeSer">       
      <option value="">Selecione</option>
      <option value="Estúdio" >Estúdio</option>
      <option value="1" >1</option>
      <option value="2" >2</option>
      <option value="3" >3</option>
      <option value="4" >4</option>
      <option value="5" >5</option>
      <option value="6" >6</option>
      <option value="7" >7</option>
      <option value="8" >8</option>
      <option value="9" >9</option>
      <option value="10" >10</option>
    </select>
  </div>
</div>
<div class="control-group">
  <label class="control-label" for="selImoTemp">Camas</label>
  <div class="controls">
    <select id="selImoTemp">       
      <option value="">Selecione</option>
      <option value="1" >1</option>
      <option value="2" >2</option>
      <option value="3" >3</option>
      <option value="4" >4</option>
      <option value="5" >5</option>
      <option value="6" >6</option>
      <option value="7" >7</option>
      <option value="8" >8</option>
      <option value="9" >9</option>
      <option value="10" >10</option>
    </select>
  </div>
</div>
<div class="control-group">
  <label class="control-label" for="garagem">Vagas de Garagem</label>
  <div class="controls">
    <input id="garagem" name="garagem" type="text">
  </div>
</div>
<div class="control-group">
  <label class="control-label" for="aluguel">Aluguel</label>
  <div class="controls">
    <input id="aluguel" name="aluguel" type="text">
    <select id="selImoTemp">       
      <option value="">Selecione</option>
      <option value="Por dia" >Por dia</option>
      <option value="Por semana" >Por semana</option>
      <option value="Por mês" >Por mês</option>
      <option value="Pacote" >Pacote</option>
    </select>
  </div>
</div>