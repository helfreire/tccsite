 <!--- Eletrônicos e telefonia - Celulares e assesórios -->

<div class="control-group">
  <label class="control-label" for="selEleTelCelAss">Marca</label>
  <div class="controls">
    <select id="selEleTelCelAss">       
      <option value="">Selecione a marca</option>
      <option value="Acessórios" >Acessórios</option>
      <option value="Apple - iPhone" >Apple - iPhone</option>
      <option value="BlackBerry" >BlackBerry</option>
      <option value="LG" >LG</option>
      <option value="Motorola" >Motorola</option>
      <option value="Nextel" >Nextel</option>
      <option value="Nokia" >Nokia</option>
      <option value="Samsung" >Samsung</option>
      <option value="Sony Ericsson" >Sony Ericsson</option>
      <option value="Telefonia" >Telefonia</option>
      <option value="other" >Outros</option>
    </select>
  </div>
</div>