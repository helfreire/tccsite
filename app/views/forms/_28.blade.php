<!--- Veículos e barcos - Carros -->

<div class="control-group">
  <label class="control-label" for="selEmpNeSer">Marca</label>
  <div class="controls">
    <select id="selEmpNeSer">       
      	<option value="" label=" "> </option>
    <optgroup id="marca-optgroup- " label=" ">
	    <option value="1" label="AUDI">AUDI</option>
	    <option value="15" label="BMW">BMW</option>
	    <option value="338" label="CHEVROLET">CHEVROLET</option>
	    <option value="39" label="CITROEN">CITROEN</option>
	    <option value="4" label="FIAT">FIAT</option>
	    <option value="272" label="FORD">FORD</option>
	    <option value="21" label="HONDA">HONDA</option>
	    <option value="22" label="HYUNDAI">HYUNDAI</option>
	    <option value="10" label="KIA">KIA</option>
	    <option value="7" label="MERCEDES-BENZ">MERCEDES-BENZ</option>
	    <option value="12" label="MITSUBISHI">MITSUBISHI</option>
	    <option value="14" label="NISSAN">NISSAN</option>
	    <option value="30" label="PEUGEOT">PEUGEOT</option>
	    <option value="537" label="RELY">RELY</option>
	    <option value="8" label="RENAULT">RENAULT</option>
	    <option value="545" label="SHINERAY">SHINERAY</option>
	    <option value="11" label="SUZUKI">SUZUKI</option>
	    <option value="37" label="TOYOTA">TOYOTA</option>
	    <option value="105" label="TRIUMPH">TRIUMPH</option>
	    <option value="6" label="VOLKSWAGEN">VOLKSWAGEN</option>
	    <option value="38" label="VOLVO">VOLVO</option>
    </optgroup>
    <optgroup id="marca-optgroup-  " label="  ">
	    <option value="330" label="ACURA">ACURA</option>
	    <option value="275" label="ADAMO">ADAMO</option>
	    <option value="2" label="ALFA ROMEO">ALFA ROMEO</option>
	    <option value="499" label="AMERICAR">AMERICAR</option>
	    <option value="109" label="ASTON MARTIN">ASTON MARTIN</option>
	    <option value="398" label="AUSTIN">AUSTIN</option>
	    <option value="77" label="BENTLEY">BENTLEY</option>
	    <option value="334" label="BRM">BRM</option>
	    <option value="80" label="BUGGY">BUGGY</option>
	    <option value="235" label="BUICK">BUICK</option>
	    <option value="55" label="CADILLAC">CADILLAC</option>
	    <option value="336" label="CBT">CBT</option>
	    <option value="120" label="CHAMONIX">CHAMONIX</option>
	    <option value="273" label="CHANA">CHANA</option>
	    <option value="337" label="CHANGHE">CHANGHE</option>
	    <option value="399" label="CHERY">CHERY</option>
	    <option value="9" label="CHRYSLER">CHRYSLER</option>
	    <option value="20" label="DAEWOO">DAEWOO</option>
	    <option value="13" label="DAIHATSU">DAIHATSU</option>
	    <option value="176" label="DKW">DKW</option>
	    <option value="17" label="DODGE">DODGE</option>
	    <option value="410" label="EFFA">EFFA</option>
	    <option value="50" label="ENGESA">ENGESA</option>
	    <option value="45" label="ENVEMO">ENVEMO</option>
	    <option value="19" label="FERRARI">FERRARI</option>
	    <option value="344" label="GEO">GEO</option>
	    <option value="353" label="GREAT WALL">GREAT WALL</option>
	    <option value="60" label="GURGEL">GURGEL</option>
	    <option value="415" label="HAFEI">HAFEI</option>
	    <option value="441" label="HOFSTETTER">HOFSTETTER</option>
	    <option value="377" label="HUMMER">HUMMER</option>
	    <option value="167" label="INFINITI">INFINITI</option>
	    <option value="23" label="ISUZU">ISUZU</option>
	    <option value="486" label="JAC MOTORS">JAC MOTORS</option>
	    <option value="24" label="JAGUAR">JAGUAR</option>
	    <option value="25" label="JEEP">JEEP</option>
	    <option value="429" label="JINBEI">JINBEI</option>
	    <option value="26" label="JPX">JPX</option>
	    <option value="428" label="KIN MOTORS">KIN MOTORS</option>
	    <option value="27" label="LADA">LADA</option>
	    <option value="94" label="LAMBORGHINI">LAMBORGHINI</option>
	    <option value="28" label="LAND ROVER">LAND ROVER</option>
	    <option value="97" label="LEXUS">LEXUS</option>
	    <option value="379" label="LEYLAND MOTORS">LEYLAND MOTORS</option>
	    <option value="447" label="LIFAN MOTORS">LIFAN MOTORS</option>
	    <option value="352" label="LINCOLN">LINCOLN</option>
	    <option value="300" label="LOBINI">LOBINI</option>
	    <option value="98" label="LOTUS">LOTUS</option>
	    <option value="325" label="MAHINDRA">MAHINDRA</option>
	    <option value="53" label="MASERATI">MASERATI</option>
	    <option value="99" label="MATRA">MATRA</option>
	    <option value="29" label="MAZDA">MAZDA</option>
	    <option value="192" label="MERCURY">MERCURY</option>
	    <option value="376" label="MG">MG</option>
	    <option value="229" label="MINI">MINI</option>
	    <option value="67" label="MIURA">MIURA</option>
	    <option value="463" label="MORRIS GARAGE">MORRIS GARAGE</option>
	    <option value="64" label="MP">MP</option>
	    <option value="498" label="NASH RAMBLER">NASH RAMBLER</option>
	    <option value="443" label="NEVIO BRENDLER">NEVIO BRENDLER</option>
	    <option value="149" label="OPEL">OPEL</option>
	    <option value="503" label="PAGANI">PAGANI</option>
	    <option value="358" label="PLYMOUTH">PLYMOUTH</option>
	    <option value="31" label="PONTIAC">PONTIAC</option>
	    <option value="32" label="PORSCHE">PORSCHE</option>
	    <option value="116" label="PROTOTIPO">PROTOTIPO</option>
	    <option value="40" label="PUMA">PUMA</option>
	    <option value="111" label="SANTA MATILDE">SANTA MATILDE</option>
	    <option value="34" label="SATURN">SATURN</option>
	    <option value="35" label="SEAT">SEAT</option>
	    <option value="71" label="SHELBY">SHELBY</option>
	    <option value="479" label="SHERCO">SHERCO</option>
	    <option value="362" label="SHINERAY">SHINERAY</option>
	    <option value="396" label="SIMCA">SIMCA</option>
	    <option value="56" label="SMART">SMART</option>
	    <option value="102" label="SSANGYONG">SSANGYONG</option>
	    <option value="454" label="SSFIBERGLASS">SSFIBERGLASS</option>
	    <option value="449" label="STUDEBAKER">STUDEBAKER</option>
	    <option value="36" label="SUBARU">SUBARU</option>
	    <option value="456" label="TAC">TAC</option>
	    <option value="47" label="TROLLER">TROLLER</option>
	    <option value="378" label="WAKE">WAKE</option>
	    <option value="365" label="WALK">WALK</option>
	    <option value="201" label="WAY">WAY</option>
	    <option value="49" label="WILLYS">WILLYS</option>
    </optgroup>
    </select>
  </div>
</div>