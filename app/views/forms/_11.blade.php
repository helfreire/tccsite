 <!--- Empregos e negócios - Ofertas de Empregos -->

<div class="control-group">
  <label class="control-label" for="empname">Nome da Empresa *</label>
  <div class="controls">
    <input id="empname" name="empname" type="text">
  </div>
</div>
<div class="control-group">
  <label class="control-label" for="SelEmpNegTivaga">Tipo de Vaga:</label>
  <div class="controls">
    <select id="SelEmpNegTivaga">       
      <option value="Temporário" >Temporário</option>
      <option value="Emprego" selected="selected">Emprego</option>
    </select>
  </div>
</div>
<div class="control-group">
  <label class="control-label" for="SelEmpNegAreAtu">Área de atuação:</label>
  <div class="controls">
    <select id="SelEmpNegAreAtu">       
      <option value="">Selecione a área de atuação</option>
      <option value="Administração - Secretária" >Administração - Secretária</option>
      <option value="Animador para festas e eventos - Ator" >Animador para festas e eventos - Ator</option>
      <option value="Atendimento ao cliente" >Atendimento ao cliente</option>
      <option value="Construção civil" >Construção civil</option>
      <option value="Contabilidade - Finanças" >Contabilidade - Finanças</option>
      <option value="Direito - Advocacia" >Direito - Advocacia</option>
      <option value="Educação - Professor" >Educação - Professor</option>
      <option value="Engenharia - Arquitetura" >Engenharia - Arquitetura</option>
      <option value="Hotelaria - Turismo - Restaurante" >Hotelaria - Turismo - Restaurante</option>
      <option value="Imobiliária" >Imobiliária</option>
      <option value="Industrial" >Industrial</option>
      <option value="Internet - Multimídia" >Internet - Multimídia</option>
      <option value="Marketing - Publicidade" >Marketing - Publicidade</option>
      <option value="Medicina - Enfermaria" >Medicina - Enfermaria</option>
      <option value="Recursos Humanos" >Recursos Humanos</option>
      <option value="Tecnologia - Informática - Programação" >Tecnologia - Informática - Programação</option>
      <option value="Varejo" >Varejo</option>
      <option value="Vendas" >Vendas</option>
      <option value="other" >Outros</option>
    </select>
  </div>
</div>