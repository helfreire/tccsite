<!--- Imóveis - Temporada -->

<div class="control-group">
  <label class="control-label" for="selEmpNeSer">Ano</label>
  <div class="controls">
    <select id="selEmpNeSer">       
      <option value="">Escolher</option>
      @for($i = date("Y"); $i > 1950; $i--)
      <option value="{{$i}}">{{ $i }} </option>
      @endfor
      <option value="1950">1950 ou menor </option>
    </select>
  </div>
</div>