 <!--- Imóveis - Terrenos - Sítios e Fazendas -->

<div class="control-group">
  <label class="control-label" for="area">Área construída</label>
  <div class="controls">
    <input id="area" name="area" type="text"> m²
  </div>
</div>
<div class="control-group">
  <label class="control-label" for="vagas">Vagas na garagem</label>
  <div class="controls">
    <input id="vagas" name="vagas" type="text">
  </div>
</div>
<div class="control-group">
  <label class="control-label" for="condominio">Condomínio (R$)</label>
  <div class="controls">
    <input id="condominio" name="condominio" type="text">,00
  </div>
</div>
<div class="control-group">
  <label class="control-label" for="iptu">IPTU (R$)</label>
  <div class="controls">
    <input id="iptu" name="iptu" type="text">,00
  </div>
</div>