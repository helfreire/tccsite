<div id="page-header" class="clearfix" >
    <div id="page-header-wrapper" class="clearfix">
        
        <div class="btn vertical-button  float-left" title="">
            <i class="glyph-icon icon-linux font-size-35 opacity-60" style="color: white"></i>
            <span class="font-white font-bold text-transform-upr pad10A font-size-16">E-cliques</span>
        </div>
        <div class="top-icon-bar">
            <a href="{{ URL::to('publicar')}}" class="btn vertical-button" title="" style="width: 300px;" >
                <span class="button-content font-gray-dark font-bold text-transform-upr font-size-12">Publicar</span>
            </a>
            <a href="{{ URL::to('login') }}" class="btn vertical-button" title="Acessar área de usuário" style="width: 300px;" >
                <span class="button-content font-gray-dark font-bold text-transform-upr font-size-12">
                    @if ( Sentry::check())
                        Gerênciar
                    @else
                        Logar
                    @endif
                    </span>
            </a>
        @if ( Sentry::check())
            @if( Session::get('contpost') > 0 )
            <a href="javascript:;" class="popover-button" data-placement="bottom" title="Mensagens de compradores" data-id="#msg-box">
                <span class="badge badge-absolute gradient-orange">{{ Session::get('contpost') }}</span>
                <i class="glyph-icon icon-envelope-alt"></i>
            </a>
            
            <div id="msg-box" class="hide">
                <div class="scrollable-content scrollable-small">
                    <ul class="no-border messages-box">
                        @foreach($posts as $post)
                            @if ( !empty( $post->posts_desc ) ) 
                            @if( $post->posts_desc[0]->action == 'question' )
                            <li>
                                <div class="messages-img">
                                    <img width="32" src="assets/images/gravatar.jpg" alt="">
                                </div>
                                <div class="messages-content">
                                    <div class="messages-title">
                                        <i class="glyph-icon icon-warning-sign font-red"></i>
                                        <a href="mensagens/{{{ $post->posts_desc[0]->id }}}" title="Message title">{{{ $post->title }}}</a>
                                        <div class="messages-time">
                                            {{{ $post->posts_desc[0]->created_at }}}
                                            <span class="glyph-icon icon-time"></span>
                                        </div>
                                    </div>
                                    <div class="messages-text">
                                        {{{ $post->posts_desc[0]->comment }}}
                                    </div>
                                </div> 
                            </li>
                            @endif
                            @endif
                        @endforeach
                        
                    </ul>

                </div>
                <div class="pad10A button-pane button-pane-alt">
                    <a href="{{ URL::to('mensagens') }}" class="btn small float-left solid-gray">
                        <span class="button-content text-transform-upr font-size-11">Todas mensagens</span>
                    </a>
                    
                  <!--  <a href="javascript:$;" class="small btn solid-red float-right mrg10R tooltip-button" data-placement="left" title="Remove comment">
                        <i class="glyph-icon icon-remove"></i>
                    </a> -->
                </div>

            </div>
            @else
                <a href="javascript:;" class="popover-button" data-placement="bottom" title="Mensagens de compradores" data-id="#msg-box">
                <i class="glyph-icon icon-envelope-alt"></i>
            </a>

            @endif
          <!--  <a href="javascript:;" class="popover-button" data-placement="bottom" title="" data-id="#notif-box">
                <span class="badge badge-absolute gradient-green">5</span>
                <i class="glyph-icon icon-bell"></i>
            </a> -->
            <div id="notif-box" class="hide">
               
                <div class="popover-title display-block clearfix form-row pad10A">
                    <div class="form-input">
                        <div class="form-input-icon">
                            <i class="glyph-icon icon-search transparent"></i>
                            <input type="text" placeholder="Search notifications..." class="radius-all-100" name="" id="">
                        </div>
                    </div>
                </div>
                <div class="scrollable-content scrollable-small">

                    <ul class="no-border notifications-box">
                        <li>
                            <span class="btn gradient-red icon-notification glyph-icon icon-user"></span>
                            <span class="notification-text">This is an error notification</span>
                            <div class="notification-time">
                                a few seconds ago
                                <span class="glyph-icon icon-time"></span>
                            </div>
                        </li>
                        <li>
                            <span class="btn solid-orange icon-notification glyph-icon icon-user"></span>
                            <span class="notification-text">This is a warning notification</span>
                            <div class="notification-time">
                                <b>15</b> minutes ago
                                <span class="glyph-icon icon-time"></span>
                            </div>
                        </li>
                        <li>
                            <span class="solid-green btn icon-notification glyph-icon icon-user"></span>
                            <span class="notification-text font-green font-bold">A success message example.</span>
                            <div class="notification-time">
                                <b>2 hours</b> ago
                                <span class="glyph-icon icon-time"></span>
                            </div>
                        </li>
                        <li>
                            <span class="btn gradient-red icon-notification glyph-icon icon-user"></span>
                            <span class="notification-text">This is an error notification</span>
                            <div class="notification-time">
                                a few seconds ago
                                <span class="glyph-icon icon-time"></span>
                            </div>
                        </li>
                        <li>
                            <span class="btn solid-orange icon-notification glyph-icon icon-user"></span>
                            <span class="notification-text">This is a warning notification</span>
                            <div class="notification-time">
                                <b>15</b> minutes ago
                                <span class="glyph-icon icon-time"></span>
                            </div>
                        </li>
                        <li>
                            <span class="solid-blue btn icon-notification glyph-icon icon-user"></span>
                            <span class="notification-text font-blue">Alternate notification styling.</span>
                            <div class="notification-time">
                                <b>2 hours</b> ago
                                <span class="glyph-icon icon-time"></span>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="pad10A button-pane button-pane-alt text-center">
                    <a href="notifications.html" class="btn medium primary-bg">
                        <span class="button-content">View all notifications</span>
                    </a>
                </div>
            </div>

            <!-- if aqui -->


            <div class="top-icon-bar dropdown">
                <a href="javascript:;" title="" class="user-ico clearfix" data-toggle="dropdown">
                  <!--  <img width="36" src="assets/images/gravatar.jpg" alt=""> -->
                    <span>{{ Session::get('nameuser') }}</span>
                    <i class="glyph-icon icon-chevron-down"></i>
                </a>
                <ul class="dropdown-menu float-right">
                    <li>
                       
                        <a href="{{ URL::to('private') }}" title="">
                            <i class="glyph-icon icon-user mrg5R"></i>
                            Detalhes Conta
                        </a>
                    </li>
                    <li>
                        <a href="{{ URL::to('logout') }}" title="">
                            <i class="glyph-icon icon-signout font-size-13 mrg5R"></i>
                            <span class="font-bold">Logout</span>
                        </a>
                    </li>
                </ul>
            </div>
            <!-- if aqui -->
            @endif
        </div>
    </div>
</div>