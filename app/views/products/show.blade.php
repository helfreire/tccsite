@extends('layouts.template')

@section ( 'script' )
  {{ HTML::script('assets/viewimage/js/jquery.elevateZoom-2.5.5.min.js') }}
  <script type="text/javascript">
      $(document).ready(function(){
          $("#zoom").elevateZoom({
                    gallery : "gallery",
                    galleryActiveClass: "active", zoomType        : "inner", cursor: "url(images/lupa-clientes.cur), auto" });

          $('#gallery a').removeClass('active').eq(currentValue-1).addClass('active');          
          var ez =   $('#zoom').data('elevateZoom');    
           
          ez.swaptheimage(smallImage, largeImage); 

      });

  </script> 

@stop
   <style type="text/css">
    #gallery_01 img{border:2px solid white; }
    .active img{border:2px solid #333 !important;}
  </style>
@stop

@section ( 'main' )

  @if( !is_null( Session::get('errorpost') ) )
    @if ( !Session::get('errorpost') )
    <div class="row">
      <div class="col-md-12 col-md-offset-1">
          <div id"div" style="height: 50px; line-height: 50px; font-weight:bold; font-size: 18px;" class="solid-green small radius-all-2 display-block btn" >
            {{ "Mensagem adicionada com sucesso" }}</div>
      </div>
    </div> 
    @endif
  @endif
  
<?php Session::forget('errorpost'); ?>  

<div class="row">
 
      <div class="row">
        @include ( 'products._product_images' )
       
        <div class="col-md-3 content-box float-left" sytle="position:absolute" >
          <div >
              <h4>R$ {{{ $products->price }}}</h4>
              <p>Preço</p>
          </div>
          <h4>{{{ $products->created_at }}}</h4>
           <p>Data de publicação</p>
        
            <h4>Anunciante</h4>
           <p>{{{ $products->seller->firstname }}} {{{ $products->seller->lastname }}}</p>
           <p>Cadastrado em : {{{ $products->created_at }}}</p>
           <br>
          <h4>Detalhes anúncio</h4>
           <p>{{{ $products->description }}}</p>
           <p>Município - {{{ $products->city }}}</p>
          </div>  
      </div>
      <div class="row">

            <div style="width:90%">
      <div class="content-box">
          <h3 class="content-box-header primary-bg">
              <div class="glyph-icon icon-separator">
                  <i class="glyph-icon icon-comments"></i>
              </div>
              <span>Deixe uma mensagem para o vendedor</span>
          </h3>
          <div class="content-box-wrapper">

              <ul class="chat-box">
                @foreach ($posts as $post )
                  <li class="{{ $post->action == 'question' ? 'float-left' : ''; }}" >
                      <div class="chat-author">
                          <img width="36" src="assets/images/gravatar.jpg" alt="">
                      </div>
                      <div class="popover {{ $post->action == 'question' ? 'right' : 'left'; }} no-shadow">
                          <div class="arrow"></div>
                          <div class="popover-content">
                                  <h3>
                                      <a href="#" title="{{{ $post->name }}}">{{{ $post->name }}}</a>
                                  </h3>
                                    {{{ $post->comment }}}
                                  <div class="chat-time">
                                      <i class="glyph-icon icon-time"></i>
                                      {{{ $post->created_at }}}
                                  </div>
                          </div>
                      </div>
                  </li>
                @endforeach
              </ul>
          </div>
          <div class="button-pane pad10A">
            {{ Form::open( array( 'url' => 'post/setpostsbuyer', 'class' => 'col-md-10 center-margin' ) ) }}
            
              <div class="form-row pad10A">
                  <div class="form-label col-md-3">
                      <label for="">
                          Nome
                      </label>
                  </div>
                  <div class="form-input col-md-9">
                      <div class="append-left">
                          {{ Form::text('name'); }}
                      </div>
                  </div>
              </div>
              <div class="form-row pad10A">
                  <div class="form-label col-md-3">
                      <label for="">
                          Email
                      </label>
                  </div>
                  <div class="form-input col-md-9">
                      <div class="append-left">
                          {{ Form::text('email', '', array( 'placeholder' => 'Deixe seu e-mail para receber resposta' ) ); }}
                      </div>
                  </div>
              </div>
              <div class="form-row pad10A">
                  <div class="form-label col-md-3">
                      <label for="">
                          Mensagem
                      </label>
                  </div>
                  <div class="form-input col-md-9">
                      <div class="append-left">
                        {{ Form::textarea('comment', '', array( 'id' => 'comment', 'placeholder' => 'Deixe sua mensagem aqui'  ) ); }}
                        
                          </div>
                      </div>
                  </div>
              </div>
              <div class="form-row pad10A">
                  <div class="form-input col-md-9">
                      <div class="append-left">
                        {{ Form::input('hidden', 'product_id', $products->id ); }}
                        {{ Form::submit('Enviar mensagem', array( 'class' => 'col-lg-3  btn primary-bg float-left') ); }}
                        
                          </div>
                      </div>
                  </div>
              </div>
            {{ Form::close() }}
          </div>
      </div>

      </div>
</div>
@stop