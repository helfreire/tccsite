<tr>
    <td>
        <div class="float-left">
            <div class="chat-author">
                {{ HTML::image($product->images->image . 'thumb/image1.png') }}
            </div>
        </div>
        <div class="float-left" style="margin: 5%;">
            <a href=" {{ URL::to($route, $product->id) }}">
                {{{ $product->title }}}
            </a> 
        </div>
    </td>
    <td>
        <p>	{{{ $product->city }}} </p>
        <p>	
            {{{ $product->categories[0]->category_name }}}  
            -  
            {{{ $product->subcategories[0]->subcategory_name}}}
        </p>
    </td>
    <td>
        R$ {{{ $product->price }}}
    </td>
    <td class="text-center">
        {{{ $product->created_at }}}
    </td>
</tr>