@extends('layouts.template')

@section ( 'main' )
<div class="row">
    
    <div >
        <h3>Produtos</h3>
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>Produto</th>
                        <th>Local</th>
                        <th>Preço</th>
                        <th>Cadastrado</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach( $products as $product )
                        @include ( 'products._product_title' )
                    @endforeach
                </tbody>
            </table>
    </div>
</div>

@stop