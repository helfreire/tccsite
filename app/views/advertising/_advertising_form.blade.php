Falta fazer : 
<p>Carros</p>
<p>Motos</p>
<form class="form-horizontal">
    <fieldset>
        <legend>Anunciar grátis</legend>
        <div class="control-group">
            <label class="control-label" for="categorias">Categoria *</label>
            <div class="controls">
                <select name="categorias" data-placeholder="Selecione.." class="chzn-select" style="width:370px;" tabindex="2">
                    <option value=""></option> 
                    @foreach($categories as $category_key => $category_name)
                    @foreach( $category_name as $subcategory_key => $subcategory_value )
                    <optgroup label='{{ $subcategory_key }}'>
                        @foreach ( $subcategory_value as $key => $name )
                        <option value="{{ $key }}">{{ $name }}</option>
                        @endforeach     
                    </optgroup>
                    @endforeach
                    @endforeach
                </select>
            </div>
        </div>
        <div id="multform"></div>
        <div class="control-group">
            <label class="control-label" for="title">Titulo *</label>
            <div class="controls">
                <input id="title" name="title" type="text">
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="descryption">Descrição *</label>
            <div class="controls">
                <textarea class="input-xlarge" id="descryption" rows="5"></textarea>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="price">Preço *</label>
            <div class="controls">
                <input id="price" name="price" type="text">
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="cep">CEP *</label>
            <div class="controls">
                <input id="cep" name="cep" type="text">
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="state">Estado *</label>
            <div class="controls">
                <input id="state" name="state" type="text">
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="city">Cidade *</label>
            <div class="controls">
                <input id="city" name="city" type="text">
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="city">Endereço</label>
            <div class="controls">
                <input class="input-large" id="address" name="address" type="text">
            </div>
        </div>
    </fieldset>
    <input type="hidden" name="site" value="{{ URL::previous() }}">
</form>