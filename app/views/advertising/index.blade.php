@extends('layouts.template')

@section ( 'script' )

  {{ HTML::script('assets/chosen/chosen.jquery.js') }}
  {{ HTML::script('assets/js/multi/jquery.MultiFile.js') }}

  <script>
    $( document ).ready(function() {
       // $(".chzn-select").chosen();
       $('select[name="categorias"]').change(function (){
           // alert($('input[name="site"]').val() + '/publicar/forms/' + $(this).val());
            $('div#multform').load( $('input[name="site"]').val() + '/publicar/forms/' + $(this).val());
       });
    });

  </script>

   
   {{ HTML::style('assets/css/basic.css') }}

@stop

@section ( 'main' )

<?php


$arrEstados = array(  'AM' => 'Amazonas', 'AC' => 'Acre', 'AL' => 'Alagoas', 'AP' => 'Amapá',
                      'CE' => 'Ceará', 'DF' => 'Distrito federal', 'ES' => 'Espirito santo', 'MA' => 'Maranhão',
                      'PR' => 'Paraná', 'PE' => 'Pernambuco', 'PI' => 'Piauí', 'RN' => 'Rio grande do norte',
                      'RS' => 'Rio grande do sul', 'RO' => 'Rondônia', 'RR' => 'Roraima',
                      'SC' => 'Santa catarina', 'SE' => 'Sergipe', 'TO' => 'Tocantins',
                      'PA' => 'Pará', 'BH' => 'Bahia', 'GO' => 'Goiás', 'MT' => 'Mato grosso',
                      'MS' => 'Mato grosso do sul', 'RJ' => 'Rio de janeiro', 'SP' => 'São paulo',
                      'RS' => 'Rio grande do sul', 'MG' => 'Minas gerais', 'PB' => 'Paraiba',
                        );

    //dd($_SESSION);

 ?>
@if( !is_null( Session::get('errorpost') ) )
    @if ( !Session::get('errorpost') )
    <div class="row">
      <div class="col-md-8 col-md-offset-1">
          <div id"div" style="height: 50px; line-height: 50px; font-weight:bold; font-size: 18px;" class="solid-green small radius-all-2 display-block btn" >
            Produto cadastrado com sucesso, 
            <a href="{{ URL::to( Session::get('country') . '/' . Session::get('product_id') ) }}" >clique aqui para visualizá-lo</a>
            
            </div>
      </div>
    </div> 
    @endif
  @endif

<h3>Publicar anúncio grátis</h3>

@if ( $errors->count() > 0 )
  <p>The following errors have occurred:</p>

  <ul>  
    @foreach( $errors->all() as $message )
      <li>{{ $message }}</li>
    @endforeach
  </ul>
@endif


@if( isset($create->error) )
  <div class="row">
    <div class="col-md-8 col-md-offset-1">
        <div  style="height: 50px; line-height: 50px; font-weight:bold; font-size: 18px;" class="gradient-green small radius-all-2 display-block btn" >
         Produto cadastrado com sucesso para visualiza-lo
        </div>
    </div>
  </div>

@endif

{{ Form::open( array( 'route' => 'publicar.store', 'class' => 'col-md-12 center-margin','files' => true ) ) }}
      <h3>Destalhes produto</h3>
    <div class="form-row">
        <div class="form-label col-md-2">
            <label for="categorias">
                Categoria:
                <span class="required">*</span>
            </label>
        </div>
        <div class="form-input col-md-5">
            <select name="categorias" data-placeholder="Selecione.." class="chosen-select" tabindex="2">
                <option value=""></option> 
              @foreach($categories as $category_key => $category_name)
                @foreach( $category_name as $subcategory_key => $subcategory_value )
                  <optgroup label='{{ $subcategory_key }}'>
                    @foreach ( $subcategory_value as $key => $name )
                      <option value="{{ $key }}">{{ $name }}</option>
                    @endforeach     
                  </optgroup>
                @endforeach
              @endforeach
            </select>
        </div>
    </div>
    <div class="form-row">
        <div class="form-label col-md-2">
            <label for="title">
                Titulo
                <span class="required">*</span>
            </label>
        </div>
        <div class="form-input col-md-5">
            {{ Form::text( 'title' , '', array( 'id' => 'title' )) }}
        </div>
    </div>
    <div class="form-row">
        <div class="form-label col-md-2">
            <label for="description">
                Descrição
                <span class="required">*</span>
            </label>
        </div>
        <div class="form-input col-md-5">
            {{ Form::textarea('description', '', array( 'class' => 'input-xlarge', 'rows' => 5, 'id' => 'description' ) ); }}
        </div>
    </div>
    <div class="form-row">
        <div class="col-md-4">

        </div>
    </div>
    <div class="form-row">
        <div class="form-label col-md-2">
            <label for="price">
                Preço
                <span class="required">*</span>
            </label>
        </div>
        <div class="form-input col-md-5">
            {{ Form::text( 'price' , '', array( 'id' => 'price' )) }}
        </div>
    </div>

    <div class="form-row">
        <div class="form-label col-md-2">
            <label for="price">
                Imagem
                <span class="required">*</span>
            </label>
        </div>
        <div class="form-input col-md-5">
             {{ Form::file('file[]', array('class' => "multi" ))}}
        </div>
     
    </div>
    
    <h3>Dados de Contato</h3>

    <div class="form-row">
        <div class="form-label col-md-2">
            <label for="phone">
                Telefone
                <span class="required">*</span>
            </label>
        </div>
        <div class="form-input col-md-5">
            {{ Form::text( 'phone' , '', array( 'id' => 'phone' )) }}
        </div>
    </div>
    <div class="form-row">
        <div class="form-label col-md-2">
            <label for="mobile">
                Celular
            </label>
        </div>
        <div class="form-input col-md-5">
            {{ Form::text( 'mobile' , '', array( 'id' => 'mobile' )) }}
        </div>
    </div>
    <div class="form-row">
        <div class="form-label col-md-2">
            <label for="country">
                Estado
                <span class="required">*</span>
            </label>
        </div>
        <div class="form-input col-md-5">
            {{ Form::select('country', $arrEstados, '', array( 'id' => 'country' )); }}
        </div>
    </div>
    <div class="form-row">
        <div class="form-label col-md-2">
            <label for="city">
                Cidade
                <span class="required">*</span>
            </label>
        </div>
        <div class="form-input col-md-5">
            {{ Form::text( 'city' , '', array( 'id' => 'city' )) }}
        </div>
    </div>
    <div class="form-row">
        <div class="form-label col-md-2">
            <label for="address">
                Endereço
            </label>
        </div>
        <div class="form-input col-md-5">
            {{ Form::text( 'address' , '', array( 'id' => 'address' )) }}
        </div>
    </div>
    <div class="form-row col-md-2">
        <input type="submit" class="btn primary-bg medium" value="Cadastrar">
    </div>
   {{ Form::close(); }}  

@stop