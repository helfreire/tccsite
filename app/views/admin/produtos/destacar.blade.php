@extends('admin.layouts.layout')

@section('main')
<?php 
//dd(date('Y-m-d', strtotime("+10days")));

Session::put('product_id', $product_id); ?>

	<div class="row">


@if( !is_null( Session::get('contratar') ) )   
    <div class="row" style="margin-bottom:30px;">
      <div class="col-md-9">
          <div id"div" style="height: 50px; line-height: 50px; font-weight:bold; font-size: 18px;" class="solid-green small radius-all-2 display-block btn" >
            Produto destacado com sucesso, 
            </div>
      </div>
    </div> 
   
@endif

	    <div class="col-md-3">
			<div class="content-box mrg15B">
		        <h3 class="content-box-header gradient-green">
		            <span>Anúncio em destaque por 7 dias</span>
		        </h3>
		        <div class="content-box-wrapper" style="height: 150px;" >
		            Seu anúncio destacado por 7 dias
		        </div>
		        <a href="{{ URL::to('contratar/7') }}" class="content-box-header btn large gradient-green" title="Example dropdown"   style="text-align; margin-left:70px;" >
					<span class="button-content" >
					<strong>Contratar</strong>
					</span>
				</a>
		    </div>
	    </div>
	    <div class="col-md-3">
			<div class="content-box mrg15B">
		        <h3 class="content-box-header solid-blue">
		            <span>Anúncio em destaque por 15 dias</span>
		        </h3>
		        <div class="content-box-wrapper" style="height: 150px;">
		            Seu anúncio destacado por 15 dias
		        </div>
		        <a href="{{ URL::to('contratar/15') }}" class="content-box-header btn large gradient-blue" title="Example dropdown"   style="text-align; margin-left:70px;" >
					<span class="button-content" >
					<strong>Contratar</strong>
					</span>
				</a>
		    </div>
	    </div>
	    <div class="col-md-3">
			<div class="content-box mrg15B">
		        <h3 class="content-box-header gradient-orange">
		            <span>Anúncio em destaque 30 dias</span>
		        </h3>
		        <div class="content-box-wrapper" style="height: 150px;" >
		            Seu anúncio destacado por 30 dias
		        </div>
		        <a href="{{ URL::to('contratar/30') }}" class="content-box-header btn large gradient-orange" title="Example dropdown"   style="text-align; margin-left:70px;" >
					<span class="button-content" >

					<strong>Contratar</strong>
					</span>
				</a>
		    </div>
	    </div>
    </div>
@stop