@extends('admin.layouts.layout')

@section ( 'script' )
  {{ HTML::script('assets/chosen/chosen.jquery.js') }}
  <script>
    $(".chzn-select").chosen();
    $('select[name="categorias"]').live('change', function() {
      alert($('input[name="site"]').val() + '/publicar/forms/' + $(this).val());
      $('div#multform').load( $('input[name="site"]').val() + '/publicar/forms/' + $(this).val());
    });
  </script>

@stop

@section ( 'main' )

<?php


$arrEstados = array(  'AM' => 'Amazonas', 'AC' => 'Acre', 'AL' => 'Alagoas', 'AP' => 'Amapá',
                      'CE' => 'Ceará', 'DF' => 'Distrito federal', 'ES' => 'Espirito santo', 'MA' => 'Maranhão',
                      'PR' => 'Paraná', 'PE' => 'Pernambuco', 'PI' => 'Piauí', 'RN' => 'Rio grande do norte',
                      'RS' => 'Rio grande do sul', 'RO' => 'Rondônia', 'RR' => 'Roraima',
                      'SC' => 'Santa catarina', 'SE' => 'Sergipe', 'TO' => 'Tocantins',
                      'PA' => 'Pará', 'BH' => 'Bahia', 'GO' => 'Goiás', 'MT' => 'Mato grosso',
                      'MS' => 'Mato grosso do sul', 'RJ' => 'Rio de janeiro', 'SP' => 'São paulo',
                      'RS' => 'Rio grande do sul', 'MG' => 'Minas gerais', 'PB' => 'Paraiba',
                        );
 ?>

<div class="row">
    <div class="col-md-12">

    @if ( $errors->count() > 0 )
  <p>The following errors have occurred:</p>

  <ul>  
    @foreach( $errors->all() as $message )
      <li>{{ $message }}</li>
    @endforeach
  </ul>
@endif

@if( isset($create->error) )
  <div class="row">
    <div class="col-md-12 col-md-offset-1">
        <div  style="height: 50px; line-height: 50px; font-weight:bold; font-size: 18px;" class="gradient-green small radius-all-2 display-block btn" >
         Produto cadastrado com sucesso para visualiza-lo
        </div>
    </div>
  </div>
@endif
<?php


?>
{{ Form::open( array( 'url' => 'produtos/atualiza', 'class' => 'col-md-10 center-margin' ) ) }}
	      <h3>Destalhes produto</h3>
	    <div class="form-row">
	        <div class="form-label col-md-2">
	            <label for="categorias">
	                Categoria:
	                <span class="required">*</span>
	            </label>
	        </div>
	        <div class="form-input col-md-5">
	        	{{ Form::select('categorias', $categories, $products[0]->subcategories[0]->id ) }}
	        </div>
	    </div>
	    <div class="form-row">
	        <div class="form-label col-md-2">
	            <label for="title">
	                Titulo
	                <span class="required">*</span>
	            </label>
	        </div>
	        <div class="form-input col-md-5">
	            {{ Form::text( 'title' , $products[0]->title, array( 'id' => 'title' )) }}
	        </div>
	    </div>
	    <div class="form-row">
	        <div class="form-label col-md-2">
	            <label for="description">
	                Descrição
	                <span class="required">*</span>
	            </label>
	        </div>
	        <div class="form-input col-md-5">
	            {{ Form::textarea('description', $products[0]->description, array( 'class' => 'input-xlarge', 'rows' => 5, 'id' => 'description' ) ); }}
	                                        </div>
	    </div>
	    <div class="form-row">
	        <div class="form-label col-md-2">
	            <label for="price">
	                Preço
	                <span class="required">*</span>
	            </label>
	        </div>
	        <div class="form-input col-md-5">
	            {{ Form::text( 'price' , $products[0]->price, array( 'id' => 'price' )) }}
	        </div>
	    </div>

	    <h3>Dados de Contato</h3>

	    <div class="form-row">
	        <div class="form-label col-md-2">
	            <label for="phone">
	                Telefone
	                <span class="required">*</span>
	            </label>
	        </div>
	        <div class="form-input col-md-5">
	            {{ Form::text( 'phone' , $products[0]->address->phone, array( 'id' => 'phone' )) }}
	        </div>
	    </div>
	    <div class="form-row">
	        <div class="form-label col-md-2">
	            <label for="mobile">
	                Celular
	            </label>
	        </div>
	        <div class="form-input col-md-5">
	            {{ Form::text( 'mobile' , $products[0]->address->mobile, array( 'id' => 'mobile' )) }}
	        </div>
	    </div>
	    <div class="form-row">
	        <div class="form-label col-md-2">
	            <label for="country">
	                Estado
	                <span class="required">*</span>
	            </label>
	        </div>
	        <div class="form-input col-md-5">
	            {{ Form::select('country', $arrEstados, $products[0]->country, array( 'id' => 'country' )); }}
	        </div>
	    </div>
	    <div class="form-row">
	        <div class="form-label col-md-2">
	            <label for="city">
	                Cidade
	                <span class="required">*</span>
	            </label>
	        </div>
	        <div class="form-input col-md-5">
	            {{ Form::text( 'city' , $products[0]->city, array( 'id' => 'city' )) }}
	        </div>
	    </div>
	    <div class="form-row">
	        <div class="form-label col-md-2">
	            <label for="address">
	                Endereço
	            </label>
	        </div>
	        <div class="form-input col-md-5">
	            {{ Form::text( 'address' , $products[0]->address->address, array( 'id' => 'address' )) }}
	        </div>
	    </div>
	    <div class="form-row col-md-2">
	        <input type="submit" class="btn primary-bg medium" value="Alterar">
	    </div>
	    {{ Form::input('hidden', 'produt_id', $products[0]->id ); }}
	   {{ Form::close(); }}
	</div>
</div>
@stop