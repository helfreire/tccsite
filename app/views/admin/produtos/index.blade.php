@extends('admin.layouts.layout')

@section('main')
	<div class="row">
        <div class="col-md-12">
        	<div>
	   	 		<table class="table ">
			        <thead>
			            <tr>
			                <th>Produto</th>
			                <th>Preço</th>
			                <th>Visualizações</th>
			                <th>Interações</th>
			            </tr>
			        </thead>
			        <tbody>
			            @include( 'admin.partials.produtos._produtos' ) 
			        </tbody>
		    	</table>
			</div>
        </div>
    </div>
@stop