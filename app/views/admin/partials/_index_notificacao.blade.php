<div class="content-box box-toggle ">
    <h3 class="content-box-header ui-state-default">
        <label class="glyph-icon icon-separator transparent">
            <i class="glyph-icon icon-comments"></i>
        </label>
        <span class="float-left">Notificações</span>
        <a href="#" class="float-right icon-separator btn ui-state-default toggle-button" title="Toggle Box">
            <i class="glyph-icon icon-toggle icon-chevron-down"></i>
        </a>
    </h3>
    <div class="content-box-wrapper">
        <div class="scrollable-content scrollable-small">

            <ul class="notifications-box">
                <li>
                    <span class="btn gradient-red icon-notification glyph-icon icon-user"></span>
                    <span class="notification-text">This is an error notification</span>
                    <div class="notification-time">
                        a few seconds ago
                        <span class="glyph-icon icon-time"></span>
                    </div>
                </li>
                <li>
                    <span class="btn solid-orange icon-notification glyph-icon icon-user"></span>
                    <span class="notification-text">This is a warning notification</span>
                    <div class="notification-time">
                        <b>15</b> minutes ago
                        <span class="glyph-icon icon-time"></span>
                    </div>
                </li>
                <li>
                    <span class="solid-blue btn icon-notification glyph-icon icon-user"></span>
                    <span class="notification-text">Alternate error notification styling using helpers.</span>
                    <div class="notification-time">
                        <b>2 hours</b> ago
                        <span class="glyph-icon icon-time"></span>
                    </div>
                </li>
                <li>
                    <span class="btn gradient-red icon-notification glyph-icon icon-user"></span>
                    <span class="notification-text">This is an error notification</span>
                    <div class="notification-time">
                        a few seconds ago
                        <span class="glyph-icon icon-time"></span>
                    </div>
                </li>
                <li>
                    <span class="btn solid-orange icon-notification glyph-icon icon-user"></span>
                    <span class="notification-text">This is a warning notification</span>
                    <div class="notification-time">
                        <b>15</b> minutes ago
                        <span class="glyph-icon icon-time"></span>
                    </div>
                </li>
                <li>
                    <span class="solid-blue btn icon-notification glyph-icon icon-user"></span>
                    <span class="notification-text">Alternate error notification styling using helpers.</span>
                    <div class="notification-time">
                        <b>2 hours</b> ago
                        <span class="glyph-icon icon-time"></span>
                    </div>
                </li>
            </ul>
        </div>
        <div class="button-pane pad10A">
            <div class="form-row pad0B text-center">
                <a href="notificacoes" class="btn medium primary-bg">
                    <span class="button-content">Visualizar todas notificações</span>
                </a>
            </div>
        </div>
    </div>
</div>