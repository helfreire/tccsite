<div id="page-sidebar" style="position: absolute">
    <div id="header-logo">
        <i class="opacity-80">Administração</i>

        <a href="javascript:;" class="tooltip-button" data-placement="bottom" title="Fechar" id="close-sidebar">
            <i class="glyph-icon icon-align-justify"></i>
        </a>
        <a href="javascript:;" class="tooltip-button hidden" data-placement="bottom" title="Abrir" id="rm-close-sidebar">
            <i class="glyph-icon icon-align-justify"></i>
        </a>
        <a href="javascript:;" class="tooltip-button hidden" title="Navigation Menu" id="responsive-open-menu">
            <i class="glyph-icon icon-align-justify"></i>
        </a>
    </div>
    <div id="sidebar-menu" class="scrollable-content">
        <ul>
            <li>
                <a href="{{ URL::to('/') }}" title="Dashboard">
                    <i class="glyph-icon icon-globe"></i>
                    Acessar site
                </a>
            </li>
            <li>
                <a href="javascript:;" title="Components">
                    <i class="glyph-icon icon-archive"></i>
                    Produtos
                </a>
                <ul>
                    <li> 
                        <a href="publicar" title="Cadastrar produto">
                            <i class="glyph-icon icon-chevron-right"></i>
                            Cadastrar
                        </a>
                    </li>
                    <!--
                    <li>
                        <a href="notifications.html" title="Notifications">
                            <i class="glyph-icon icon-chevron-right"></i>
                            Notificações
                            <span class="badge primary-bg radius-all-100">19</span>
                        </a>
                    </li>
                    <li>
                        <a href="messaging.html" title="Messaging">
                            <i class="glyph-icon icon-chevron-right"></i>
                            Mensagens
                            <span class="badge primary-bg radius-all-100">19</span>
                        </a>
                    </li>
                -->
                </ul>
            </li>
            <!--
            <li>
                <a href="javascript:;" title="Data visualization">
                    <i class="glyph-icon icon-bar-chart"></i>
                    Estatísticas
                </a>
                <ul>
                    <li>
                        <a href="charts_piegauges.html" title="Pie Gauges">
                            <i class="glyph-icon icon-chevron-right"></i>
                            Produtos Visualizados
                        </a>
                    </li>
                </ul>
            </li>   
            -->
        </ul>
        <div class="divider mrg5T mobile-hidden"></div>
        <div class="text-center mobile-hidden">
            <!-- <div class="button-group">
                <a href="javascript:;" class="btn medium primary-bg tooltip-button" data-placement="top" title="Top tooltip">
                    <i class="glyph-icon icon-flag"></i>
                </a>
                <a href="javascript:;" class="btn medium primary-bg tooltip-button" data-placement="bottom" title="Bottom tooltip">
                    <i class="glyph-icon icon-inbox"></i>
                </a>
                <a href="javascript:;" class="btn medium primary-bg tooltip-button" data-placement="right" title="Right tooltip">
                    <i class="glyph-icon icon-hdd"></i>
                </a>
            </div>
            <!--
            <div class="divider"></div>
            <div class="button-group-vertical">
                <a href="javascript:;" data-layout="center" data-type="warning" class="solid-white medium radius-all-2 display-block btn noty">
                    <span class="button-content font-orange text-left font-size-12">
                        <i class="glyph-icon icon-warning-sign opacity-80 float-left mrg10R"></i>
                        Warning
                        <i class="glyph-icon icon-caret-right font-gray-dark opacity-60 float-right"></i>
                    </span>
                </a>
                <a href="javascript:;" data-layout="bottom" data-type="information" class="solid-white medium radius-all-2 display-block btn noty">
                    <span class="button-content font-gray-dark text-left font-size-12">
                        <i class="glyph-icon icon-thumbs-up opacity-80 float-left mrg10R"></i>
                        Information
                        <i class="glyph-icon icon-caret-right font-gray-dark opacity-60 float-right"></i>
                    </span>
                </a>
                <a href="javascript:;" data-layout="top" data-type="notification" class="solid-white medium radius-all-2 display-block btn noty">
                    <span class="button-content font-blue text-left font-size-12">
                        <i class="glyph-icon icon-comments opacity-80 float-left mrg10R"></i>
                        Notification
                        <i class="glyph-icon icon-caret-right font-gray-dark opacity-60 float-right"></i>
                    </span>
                </a>
            </div>
        -->
        </div>
    </div>
</div><!-- #page-sidebar -->