 <!--[if lt IE 9]>
    {{ HTML::script('assets/js/minified/core/html5shiv.min.js') }}
    {{ HTML::script('assets/js/minified/core/respond.min.js') }}
  <![endif]-->

  <!--[if lt IE 7]>
  {{ HTML::style('assets/css/minified/icons-ie7.min.css') }}
  <![endif]-->

  {{ HTML::script('assets/js/jquery-1.8.3.min.js') }}

  <!-- AgileUI CSS Core -->
    {{ HTML::style('assets/css/minified/aui-production.min.css') }}
  <!-- Theme UI -->
    {{ HTML::style('assets/themes/minified/agileui/color-schemes/layouts/default.min.css') }}
    {{ HTML::style('assets/themes/minified/agileui/color-schemes/elements/default.min.css') }}
  <!-- AgileUI Responsive -->

    {{ HTML::style('assets/themes/minified/agileui/responsive.min.css') }}
  <!-- AgileUI JS -->

    {{ HTML::script('assets/js/minified/aui-production.min.js') }}