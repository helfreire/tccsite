<div class="content-box-wrapper">
    <div class="scrollable-content scrollable-xlarge">
        <ul class="notifications-box">
            <li>
                <span class="btn gradient-red icon-notification glyph-icon icon-user"></span>
                <span class="notification-text">This is an error notification</span>
                <div class="notification-time">
                    a few seconds ago
                    <span class="glyph-icon icon-time"></span>
                </div>
            </li>
            <li>
                <span class="btn solid-orange icon-notification glyph-icon icon-user"></span>
                <span class="notification-text">This is a warning notification</span>
                <div class="notification-time">
                    <b>15</b> minutes ago
                    <span class="glyph-icon icon-time"></span>
                </div>
            </li>
            <li>
                <span class="solid-blue btn icon-notification glyph-icon icon-user"></span>
                <span class="notification-text">Alternate error notification styling using helpers.</span>
                <div class="notification-time">
                    <b>2 hours</b> ago
                    <span class="glyph-icon icon-time"></span>
                </div>
            </li>
            <li>
                <span class="btn gradient-red icon-notification glyph-icon icon-user"></span>
                <span class="notification-text">This is an error notification</span>
                <div class="notification-time">
                    a few seconds ago
                    <span class="glyph-icon icon-time"></span>
                </div>
            </li>
            <li>
                <span class="btn solid-orange icon-notification glyph-icon icon-user"></span>
                <span class="notification-text">This is a warning notification</span>
                <div class="notification-time">
                    <b>15</b> minutes ago
                    <span class="glyph-icon icon-time"></span>
                </div>
            </li>
            <li>
                <span class="solid-blue btn icon-notification glyph-icon icon-user"></span>
                <span class="notification-text">Alternate error notification styling using helpers.</span>
                <div class="notification-time">
                    <b>2 hours</b> ago
                    <span class="glyph-icon icon-time"></span>
                </div>
            </li>
        </ul>
    </div>
</div>