<div class="content-box box-toggle">
    <h3 class="content-box-header ui-state-default">
        <span class="float-left">Produtos</span>
        <a href="#" class="float-right icon-separator btn ui-state-default toggle-button" title="Toggle Box">
            <i class="glyph-icon icon-toggle icon-chevron-down"></i>
        </a>
    </h3>
    <div class="content-box-wrapper">
        <!-- class="table-hover" style="width: 100%;" -->
        <table class="scrollable-content scrollable-small">
            <tbody>
                @foreach ($produtos as $produto)
                    <tr>
                    <td width="300px">
                        <div class="float-left">
                            <div class="chat-author">
                                <img width="128" src="{{{ $produto->images->image}}}large/image1.jpg" alt="">
                            </div>
                        </div>
                        <div class="float-left" style="margin: 5%;">
                         {{{ $produto->title }}}
                        </div>
                    </td>
                    <td>
                        <div class="info-box float-right " style="border: hidden;">
                            <span class="stats">
                                <span>Total de visitas</span>
                                <span class="font-green">{{ $produto->view }}</span>
                            </span>
                        </div>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <div class="button-pane pad10A" style="margin-top:2px;">
            <div class="form-row pad0B text-center">
                <a href="produtos" class="btn medium primary-bg">
                    <span class="button-content">Visualizar todos produto</span>
                </a>
            </div>
        </div>
    </div>
</div>