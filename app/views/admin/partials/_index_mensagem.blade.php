<div class="content-box box-toggle ">
    <h3 class="content-box-header ui-state-default">
        <label class="glyph-icon icon-separator transparent">
            <i class="glyph-icon icon-comments"></i>
        </label>
        <span class="float-left">Mensagens</span>
        <a href="#" class="float-right icon-separator btn ui-state-default toggle-button" title="Toggle Box">
            <i class="glyph-icon icon-toggle icon-chevron-down"></i>
        </a>
    </h3>
    
    <div class="content-box-wrapper">
        <div class="scrollable-content scrollable-small">

            <ul class="messages-box remove-border">
                @if ( Session::get('contpost') > 0 )
                @foreach($posts as $post)
                    @if ( !empty( $post->posts_desc ) ) 
                    @if( $post->posts_desc[0]->action == 'question' )
                    <li>
                        <div class="messages-img">
                            <img width="32" src="assets/images/gravatar.jpg" alt="">
                        </div>
                        <div class="messages-content">
                            <div class="messages-title">
                                <i class="glyph-icon icon-warning-sign font-red"></i>
                                <a href="mensagens/{{{ $post->posts_desc[0]->id }}}" title="Message title">{{{ $post->title }}}</a>
                                <div class="messages-time">
                                    {{{ $post->posts_desc[0]->created_at }}}
                                    <span class="glyph-icon icon-time"></span>
                                </div>
                            </div>
                            <div class="messages-text">
                                {{{ $post->posts_desc[0]->comment }}}
                            </div>
                        </div> 
                    </li>
                    @endif
                    @endif
                @endforeach
                @endif
            </ul>
        </div>
        
        <div class="button-pane pad10A">
            <div class="form-row pad0B text-center">
                <a href="mensagens" class="btn medium primary-bg">
                    <span class="button-content">Visualizar todas mensagens</span>
                </a>
            </div>
        </div>
    </div>
</div>