@foreach( $products as $product )

<tr>
    <td>
        <div class="float-left">
            <div class="chat-author">
                <img width="100" src="{{{ $product->images->image}}}/large/image1.jpg" alt="">
            </div>
        </div>
        <div class="float-left" style="margin: 5%;">
            {{{ $product->title }}}
        </div>
    </td>
    <td>R$ {{{ $product->price }}}</td>
    <td>
        <span style="font-size:14px; text-align:center;" >{{{ $product->view }}}</span>
    </td>
    <td class="text-center">
        <div class="button-group dropdown">
            <a href="{{ URL::to('destacar/' . $product->id) }}" class="btn x-large ui-state-default" title="">
                <span class="button-content">Destacar produto</span>
            </a>
            <a href="javascript:;" class="btn x-large primary-bg" data-toggle="dropdown">
                <span class="glyph-icon icon-separator float-right">
                    <i class="glyph-icon icon-angle-down"></i>
                </span>
            </a>
            <ul class="dropdown-menu float-right">
                <li>
                    <a href="{{ URL::to( 'produtos/' . $product->id ) }}" title="">
                        Editar produto
                    </a>
                </li>
                <li>
                    <a href="{{ URL::to( 'desativar/' . $product->id ) }}" title="">
                        Desativar produto
                    </a>
                </li>
               <!-- <li>
                    <a href="{{ URL::to( 'estatisticas/' . $product->id ) }}" title="">
                        Métricas
                    </a>
                </li> -->
            </ul>
        </div>
    </td>
</tr>
@endforeach