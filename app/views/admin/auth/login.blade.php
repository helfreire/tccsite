@extends('admin.layouts.layout')



@section('login')

<div class="row" >
    @if ($errors->has('login'))
        <div class="row">
            <div class="col-md-12 col-md-offset-1">
                <div  style="height: 50px; line-height: 50px; font-weight:bold; font-size: 18px;" class="gradient-orange small radius-all-2 display-block btn" >{{ $errors->first('login', ':message') }}</div>
            </div>
        </div>
    @endif
    <div class="col-md-12 col-md-offset-1 ">
        <form action="{{ URL::to('login') }}" id="login-validation" class="col-md-4 center-margin form-vertical mrg25T" method="POST" accept-charset="UTF-8">
            <div id="login-form" class="content-box drop-shadow">
                <h3 class="content-box-header ui-state-default">
                    <div class="glyph-icon icon-separator">
                        <i class="glyph-icon icon-user"></i>
                    </div>
                    <span>Sou usuário</span>
                </h3>
                <div class="content-box-wrapper pad20A pad0B">
                    <div class="form-row">
                        <div class="form-label col-md-2">
                            <label for="login">
                                Login:
                                <span class="required">*</span>
                            </label>
                        </div>
                        <div class="form-input col-md-10">
                            <div class="form-input-icon">
                                <i class="glyph-icon icon-envelope-alt ui-state-default"></i>
                                <input placeholder="Endereço de e-mail" data-type="email" data-trigger="change" data-required="true" type="text" name="email" id="login_email">
                            </div>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-label col-md-2">
                            <label for="password">
                                Senha:
                            </label>
                        </div>
                        <div class="form-input col-md-10">
                            <div class="form-input-icon">
                                <i class="glyph-icon icon-unlock-alt ui-state-default"></i>
                                <input placeholder="Senha" data-trigger="keyup" data-rangelength="[3,25]" type="password" name="password" id="login_pass">
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-checkbox-radio col-md-6 hide">
                            <input type="checkbox" class="custom-checkbox" name="remember-password" id="remember-password">
                            <label for="remember-password" class="pad5L">Remember password?</label>
                        </div>
                        <div class="form-checkbox-radio text-left col-md-6">
                            <a href="#" class="toggle-switch" switch-target="#login-forgot" switch-parent="#login-form" title="Recover password">Recuperar senha ?</a>
                        </div>
                    </div>
                </div>
                <div class="button-pane">
                    <button type="submit" class="btn large primary-bg text-transform-upr font-bold font-size-11 radius-all-4" id="demo-form-valid" title="Validar!">
                        <span class="button-content">
                            Login
                        </span>
                    </button>
                </div>
            </div>

            <div class="divider"></div>
            <div class="form-row text-center">
                <a href="{{ URL::to( 'novousuario' ) }}" class="solid-gray btn large  radius-all-100" title="Registrar">
                    <span class="button-content pad20L pad20R">
                        Registrar nova conta
                    </span>
                </a>
            </div>
            <div class="ui-dialog mrg5T hide" id="login-forgot" style="position: relative !important;">
                <div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix">
                    <span class="ui-dialog-title">Recuperar senha</span>
                </div>
                <div class="pad20A ui-dialog-content ui-widget-content">
                    <div class="form-row">
                        <div class="form-label col-md-2">
                            <label for="">
                                Email address:
                            </label>
                        </div>
                        <div class="form-input col-md-10">
                            <div class="form-input-icon">
                                <i class="glyph-icon icon-envelope-alt ui-state-default"></i>
                                <input placeholder="Endereço de email" type="text" name="rec_email" id="">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="ui-dialog-buttonpane text-center">
                    <button type="submit" class="btn large primary-bg radius-all-4" id="demo-form-valid" title="Validar!">
                        <span class="button-content">
                            Recuperar Senha
                        </span>
                    </button>
                    <a href="javascript:;" switch-target="#login-form" switch-parent="#login-forgot" class="btn large transparent no-shadow toggle-switch font-bold font-size-11 radius-all-4" id="login-form-valid" title="Validate!">
                        <span class="button-content">
                            Cancelar
                        </span>
                    </a>
                </div>
            </div>
            @if (isset($action))
                {{ Form::input('hidden', 'method', $action) }}
            @endif
        </form>
    </div>
</div>
@stop