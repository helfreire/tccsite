@extends('admin.layouts.layout')

@section('main')

<div class="row">
    <div class="col-md-3">
        <div class="content-box">
            <div class="content-box-wrapper">
                <img src="{{ URL::to($posts->images->image . '/large/image1.jpg') }}" width="100%" height="100%" >
            </div>
            <div class="divider"></div>
            <h3 class="text-center pad10A">{{{$posts->title}}}</h3>
        </div>
    </div>
    <div class="col-md-9">
            <div class="content-box">
                @foreach( $posts->posts as $comment )

                <div class="content-box-wrapper">
                    <ul class="chat-box">
                        <li class="{{ $comment->action == 'question' ? 'float-left' : ''; }}">
                            <div class="chat-author">
                                <!-- <img width="36" src="assets/images/gravatar.jpg" alt=""> -->
                            </div>
                            <div class="popover {{ $comment->action == 'question' ? 'right' : 'left'; }} no-shadow">
                                <div class="arrow"></div>
                                <div class="popover-content">
                                    {{{ $comment->comment }}}
                                    <div class="chat-time">
                                        <i class="glyph-icon icon-time"></i>
                                        {{{ $comment->created_at }}}
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
                @endforeach
                <div class="button-pane pad10A">
                    {{ Form::open( array( 'url' => 'post/responder', 'class' => 'col-md-8 center-margin' ) ) }}
                    <div class="form-row pad0B">
                        <div class="form-input">
                                    {{ Form::textarea('comment', '', array( 'id' => 'comment', 'placeholder' => 'Deixe sua mensagem aqui'  ) ); }}
                                <div class="append-right" style="margin-top: 20px;">
                                    {{ Form::submit('Enviar mensagem', array( 'class' => 'col-lg-3  btn primary-bg float-left') ) }}
                                </div>
                           
                        </div>
                    </div>
                    {{ Form::input('hidden', 'product_id', $posts->id ); }}
                    {{ Form::close() }}
                </div>
            </div>
    </div>
</div>

@stop