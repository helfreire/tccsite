@extends('admin.layouts.layout')

@section ('script')
    <script type="text/javascript">
    $(document).ready(function (){
        $( "#resposta" ).click(function() {
          $('#divresposta').removeClass("hide");
        });
    });    
    </script>
@stop

@section('main')

@foreach ( $posts  as $product)

   @if( !empty( $product->posts_desc ) )   
    @if ( $product->posts_desc[0]->action ==  'question')
    <div class="row">
        <div class="col-md-3">
            <div class="content-box">
                <div class="content-box-wrapper text-center">
                    <img src="{{ URL::to($product->images->image . '/large/image1.jpg') }}" width="50%" height="50%" >
                    <h5>{{{ $product->title }}} </h5>
                </div>
            </div>
        </div>
        <div class="col-md-9">
            <div class="content-box">
                <div class="content-box-wrapper">
                    <div class="popover-content">
                            <h4>
                              <a href="#" title="{{{ $product->posts_desc[0]->name }}}">{{{ $product->posts_desc[0]->name }}}</a>
                            </h4>
                                    {{{ $product->posts_desc[0]->comment }}}
                                    <div class="chat-time">
                                        <i class="glyph-icon icon-time"></i>
                                        {{{ $product->posts_desc[0]->created_at }}}
                                    </div>
                                    <div class="divider"></div>
                                    <a id="resposta" href="#" class="small btn solid-gray font-bold text-transform-upr" title=""><span  class="button-content">Responder</span></a>
                                    <a href="{{ URL::to('todasmensagens/' . $product->id ) }}" class="small btn solid-gray font-bold text-transform-upr" title=""><span class="button-content">Histórico</span></a>
                                    <a href="post/remover/{{{$product->posts_desc[0]->id}}}" class="small btn solid-red float-right tooltip-button" data-placement="top" title="Remover comentário"><i class="glyph-icon icon-remove"></i></a>
                                </div>
                </div>
                <div id="divresposta" class="button-pane pad10A hide ">
                    {{ Form::open( array( 'url' => 'post/responder', 'class' => 'col-md-8 center-margin' ) ) }}
                    <div class="form-row pad0B">
                        <div class="form-input">
                                    {{ Form::textarea('comment', '', array( 'id' => 'comment', 'placeholder' => 'Deixe sua mensagem aqui'  ) ); }}
                                <div class="append-right" style="margin-top: 20px;">
                                    {{ Form::submit('Enviar mensagem', array( 'class' => 'col-lg-3  btn primary-bg float-left') ) }}
                                </div>
                           
                        </div>
                    </div>
                    {{ Form::input('hidden', 'product_id', $product->id ); }}
                    {{ Form::close() }}
                </div>
            </div>

        </div>
    </div>
    @endif
    @endif
@endforeach

@stop