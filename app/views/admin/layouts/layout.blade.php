<!doctype html>
<html lang="pt">
    <head>
        <meta charset="utf-8">
        <title>Admin</title>
        @include('admin.partials._assets')
        {{ HTML::script('assets/chosen/chosen.jquery.js') }}
  <script>
    $(".chzn-select").chosen();
    $('select[name="categorias"]').live('change', function() {
      alert($('input[name="site"]').val() + '/publicar/forms/' + $(this).val());
      $('div#multform').load( $('input[name="site"]').val() + '/publicar/forms/' + $(this).val());
    });
  </script>
        @yield('script')

    </head>
    <body>
        <div id="page-wrapper">
            @if ( Sentry::check())
                @include('admin.partials._menulateral')
            @endif
            <div id="page-main-wrapper">
                @if ( Sentry::check())
                    @include('admin.partials._top')
                @endif    
                <div id="page-content">
                    @yield('main')
                </div>
            </div><!-- #page-main -->
             @if ( !Sentry::check())
                <div id="page-main">
                    <div id="page-main-wrapper">
                        <div id="page-content">
                            @yield('login')
                        </div>
                    </div><!-- #page-main -->
                </div>
            @endif 
        </div><!-- #page-wrapper -->
    </body>
</html>