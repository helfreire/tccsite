@extends('admin.layouts.layout')

@section('main')
    <div class="row">
        <div class="col-md-6">
            @include( 'admin.partials._index_produtos' ) 
        </div>
        <div class="col-md-6">
            @include( 'admin.partials._index_mensagem' ) 
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            
        </div>
        <div class="col-md-6">
          <!--  @include( 'admin.partials._index_notificacao' )  -->
        </div>
    </div>    
@stop