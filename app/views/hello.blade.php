<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Laravel PHP Framework</title>
    {{ HTML::style('assets/bootstrap/css/bootstrap.min.css') }}
    {{ HTML::style('assets/bootstrap/css/bootstrap-responsive.css') }}
    {{ HTML::script('assets/bootstrap/js/bootstrap.js') }}
    <style type="text/css">
    	li{
    		list-style: none;
    	}
    </style>
<body>

<?php
    $arrEstados = array(  	'amazonas' => 'Amazonas', 'acre' => 'Acre', 
    						'Alagoas' => 'Alagoas', 'amapa' => 'Amapá',
							'ceara' => 'Ceará', 'distritofedera' => 'Distrito federal', 
							'espiritosanto' => 'Espirito santo', 'maranhao' => 'Maranhão',
                  			'parana' => 'Paraná', 'pernambuco' => 'Pernambuco', 'piaui' => 'Piauí', 
                  			'riograndedonorte' => 'Rio grande do norte',
                  			'riograndedonsul' => 'Rio grande do sul', 'rondonia' => 'Rondônia', 
                  			'roraima' => 'Roraima','santacatarina' => 'Santa catarina', 
                  			'sergipe' => 'Sergipe', 'tocantins' => 'Tocantins',
                  			'para' => 'Pará', 'bahia' => 'Bahia', 'goias' => 'Goiás', 
                  			'matogrosso' => 'Mato grosso','matogrossodosul' => 'Mato grosso do sul', 
                  			'riodejaneiro' => 'Rio de janeiro', 'saopaulo' => 'São paulo',
                  			'riograndedosul' => 'Rio grande do sul', 'minasgerais' => 'Minas gerais', 
                  			'paraiba' => 'Paraiba',
                );
?>




    <div class="container">
    	<div class="row">
    		<div class="span12 offset2">
    				<h1>Ferramenta para consulta de produtos on-line</h1>
    		</div>
    	</div>
    	<div class="row" 	>
    		<div class="well span6 offset3">
    			<h3>Selecione a região</h3>
    			<ul>
    				<li> <a href="brasil">Brasil</a> </li>
    				@foreach($arrEstados as $key => $value)
    					<li><a href="{{$key}}">{{$value}}</a></li>
    				@endforeach
    			</ul>

    		</div>
    	</div>
    <div>
</body>
</html>
