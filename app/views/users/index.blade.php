@extends('layouts.template')

@section ( 'main' )
 
@if( !is_null( Session::get('cadastro') ) )
    <div class="row">
      <div class="col-md-12 col-md-offset-1">
          <div id"div" style="height: 50px; line-height: 50px; font-weight:bold; font-size: 18px;" class="solid-green small radius-all-2 display-block btn" >
            Usuário cadastrado com sucesso
            
            </div>
      </div>
    </div> 
    
  @endif


<?php

$arrEstados = array(  'AM' => 'Amazonas', 'AC' => 'Acre', 'AL' => 'Alagoas', 'AP' => 'Amapá',
                      'CE' => 'Ceará', 'DF' => 'Distrito federal', 'ES' => 'Espirito santo', 'MA' => 'Maranhão',
                      'PR' => 'Paraná', 'PE' => 'Pernambuco', 'PI' => 'Piauí', 'RN' => 'Rio grande do norte',
                      'RS' => 'Rio grande do sul', 'RO' => 'Rondônia', 'RR' => 'Roraima',
                      'SC' => 'Santa catarina', 'SE' => 'Sergipe', 'TO' => 'Tocantins',
                      'PA' => 'Pará', 'BH' => 'Bahia', 'GO' => 'Goiás', 'MT' => 'Mato grosso',
                      'MS' => 'Mato grosso do sul', 'RJ' => 'Rio de janeiro', 'SP' => 'São paulo',
                      'RS' => 'Rio grande do sul', 'MG' => 'Minas gerais', 'PB' => 'Paraiba',
                        );
 ?>

{{ Form::open( array( 'route' => 'novousuario.store', 'class' => 'col-md-12 center-margin' ) ) }}
      <h3>Dados pessoais</h3>

    <div class="form-row">
        <div class="form-label col-md-3">
            <label for="title">
                Nome
                <span class="required">*</span>
            </label>
        </div>
        <div class="form-input col-md-5">
            {{ Form::text( 'first_name' , '', array( 'id' => 'first_name' )) }}
        </div>
    </div>
    <div class="form-row">
        <div class="form-label col-md-3">
            <label for="title">
                Sobrenome
                <span class="required">*</span>
            </label>
        </div>
        <div class="form-input col-md-5">
            {{ Form::text( 'last_name' , '', array( 'id' => 'last_name' )) }}
        </div>
    </div>
    <div class="form-row">
        <div class="form-label col-md-3">
            <label for="title">
                Usuário
                <span class="required">*</span>
            </label>
        </div>
        <div class="form-input col-md-5">
            {{ Form::text( 'user' , '', array( 'id' => 'user' )) }}
        </div>
    </div>
    <div class="form-row">
        <div class="form-label col-md-3">
            <label for="title">
                Senha
                <span class="required">*</span>
            </label>
        </div>
        <div class="form-input col-md-5">
            {{ Form::password( 'password' , '', array( 'id' => 'password' )) }}
        </div>
    </div>
    <div class="form-row">
        <div class="form-label col-md-3">
            <label for="title">
                Redigite sua senha
                <span class="required">*</span>
            </label>
        </div>
        <div class="form-input col-md-5">
            {{ Form::password( 're_password' , '', array( 'id' => 're_password' )) }}
        </div>
    </div>
    
    <h3>Dados de Contato</h3>
    <div class="form-row">
        <div class="form-label col-md-3">
            <label for="phone">
                E-mail
                <span class="required">*</span>
            </label>
        </div>
        <div class="form-input col-md-5">
            {{ Form::text( 'email' , '', array( 'id' => 'email' )) }}
        </div>
    </div>
    <div class="form-row">
        <div class="form-label col-md-3">
            <label for="phone">
                Telefone
                <span class="required">*</span>
            </label>
        </div>
        <div class="form-input col-md-5">
            {{ Form::text( 'phone' , '', array( 'id' => 'phone' )) }}
        </div>
    </div>
    <div class="form-row">
        <div class="form-label col-md-3">
            <label for="mobile">
                Celular
            </label>
        </div>
        <div class="form-input col-md-5">
            {{ Form::text( 'mobile' , '', array( 'id' => 'mobile' )) }}
        </div>
    </div>
    <div class="form-row">
        <div class="form-label col-md-3">
            <label for="country">
                Estado
                <span class="required">*</span>
            </label>
        </div>
        <div class="form-input col-md-5">
            {{ Form::select('country', $arrEstados, '', array( 'id' => 'country' )); }}
        </div>
    </div>
    <div class="form-row">
        <div class="form-label col-md-3">
            <label for="city">
                Cidade
                <span class="required">*</span>
            </label>
        </div>
        <div class="form-input col-md-5">
            {{ Form::text( 'city' , '', array( 'id' => 'city' )) }}
        </div>
    </div>
    <div class="form-row">
        <div class="form-label col-md-3">
            <label for="address">
                Endereço
            </label>
        </div>
        <div class="form-input col-md-5">
            {{ Form::text( 'address' , '', array( 'id' => 'address' )) }}
        </div>
    </div>
    <div class="form-row col-md-2">
        <input type="submit" class="btn primary-bg medium" value="Cadastrar">
    </div>
   {{ Form::close(); }}  

@stop