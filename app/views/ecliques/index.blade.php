<?php
    $arrEstados = array(    'amazonas' => 'Amazonas', 'acre' => 'Acre', 
                            'alagoas' => 'Alagoas', 'amapa' => 'Amapá',
                            'ceara' => 'Ceará', 'distritofederal' => 'Distrito federal', 
                            'espiritosanto' => 'Espirito santo', 'maranhao' => 'Maranhão',
                            'parana' => 'Paraná', 'pernambuco' => 'Pernambuco', 'piaui' => 'Piauí', 
                            'riograndedonorte' => 'Rio grande do norte',
                            'riograndedonsul' => 'Rio grande do sul', 'rondonia' => 'Rondônia', 
                            'roraima' => 'Roraima','santacatarina' => 'Santa catarina', 
                            'sergipe' => 'Sergipe', 'tocantins' => 'Tocantins',
                            'para' => 'Pará', 'bahia' => 'Bahia', 'goias' => 'Goiás', 
                            'matogrosso' => 'Mato grosso','matogrossodosul' => 'Mato grosso do sul', 
                            'riodejaneiro' => 'Rio de janeiro', 'saopaulo' => 'São paulo',
                            'riograndedosul' => 'Rio grande do sul', 'minasgerais' => 'Minas gerais', 
                            'paraiba' => 'Paraiba',
                );
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="shortcut icon" href="../favicon.ico">
        {{ HTML::style('assets/ecliques/estilo.css') }}
        {{ HTML::style('assets/ecliques/css/default.css') }}
        {{ HTML::style('assets/ecliques/css/component.css') }}
        {{ HTML::style('assets/ecliques/font-awesome/bootstrap/css/bootstrap.min.css') }}
        {{ HTML::style('assets/ecliques/font-awesome/css/font-awesome.min.css') }}
        {{ HTML::script('assets/ecliques/js/modernizr.custom.js') }}
        <title></title>
    </head>
    <body>
        <div class="esquerda">
            <div class="cima">
                <img src="{{URL::asset('assets/ecliques/logo1.png')}}" >
            </div>
            <br><br>
            <span class="titulo1">Clique, Anuncie,<br> Compartilhe e Venda! </span>
            <BR><BR><BR><BR>
            <select id="estados" class="estados">
                <option value="brasil">Selecione o Estado</option>
                @foreach($arrEstados as $key => $value)
                    <option value="{{$key}}">{{$value}}</option>
                @endforeach
            </select>
            <br>
            <input type="button" onclick="buscar();" value="Procurar" style="width: 220px"></input>
        </div>
        <nav class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-right" id="cbp-spmenu-s2">
            <h3>Menu</h3>
            <span id="showRight"><i class="icon-home icon-3x"></i></span>   
            <a href="login">Entrar</a>
        </nav>
    </div>
    <!-- Classie - class helper functions by @desandro https://github.com/desandro/classie -->
    {{ HTML::script('assets/ecliques/js/classie.js') }}
    <script>
        var menuLeft = document.getElementById( 'cbp-spmenu-s1' ),
        menuRight = document.getElementById( 'cbp-spmenu-s2' ),
        menuTop = document.getElementById( 'cbp-spmenu-s3' ),
        menuBottom = document.getElementById( 'cbp-spmenu-s4' ),
        showLeft = document.getElementById( 'showLeft' ),
        body = document.body;
        

            
        showRight.onclick = function() {
            classie.toggle( this, 'active' );
            classie.toggle( menuRight, 'cbp-spmenu-open' );
            disableOther( 'showRight' );
        };
        
        
        function buscar(){
            var estado = document.getElementById("estados").value;
             location.href = estado;
        }
    
    </script>
</body>
</html>