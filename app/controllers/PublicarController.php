<?php

class PublicarController extends BaseController {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {

//date('d/m/Y', strftime("+10 days"));

        $tccurl = new \TccCurl( 'private/getpost' );
            $tccurl->post( ['seller_id' => Sentry::getIdUser() ] );
            $posts = $tccurl->request();
        if ( !Sentry::check())
                return View::make('admin.auth.login', ['posts' => $posts, 
                                                        'action' => 'publicar',
                                                        'route' => 'brasil']);

        return View::make( 'advertising.index', ['categories' => $this->categorias(), 
                                                                    'posts' => $posts,
                                                                    'route'  => 'brasil']);
    }

    public function categorias(){
        $tccurl = new TccCurl('advertising');
        return $tccurl->request();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {

        $rules = array(
            'categorias'    => 'required',
            'title'         => 'required',
            'description'   => 'required',
            'price'         => 'required',
            'phone'         => 'required',
            'country'       => 'required',
            'city'          => 'required'
        );

        /*
        $validation = Validator::make( Input::all(), $rules);

        if ($validation->fails())
        {
            return Redirect::back()->withErrors($validation);
            
        }
        */
        
        $path = '/var/www/images/product';
        $tccurl = new TccCurl('advertising/create');
        $tccurl->post( array_merge(Input::all(), ['seller_id' => Sentry::getIdUser(), 'quantity' => count(Input::file('file')) ] ) );
        $create = $tccurl->request();
        
        $dirlarge = $path . '-' . $create->product_id . '/large/';
        $dirsmall = $path . '-' . $create->product_id . '/small/';
        $dirthumb = $path . '-' . $create->product_id . '/thumb/';

        mkdir( $dirlarge, 0777, true);
        mkdir( $dirsmall, 0777, true);
        mkdir( $dirthumb, 0777, true);
        

        $files = Input::file('file');
        if($files){
            $i = 1;
            foreach ( $files as $file) {
                Image::make( $file->getRealPath())->resize( 800, 600 )
                            ->save( $dirlarge . 'image' . $i . '.jpg' );
                Image::make( $file->getRealPath())->resize( 411, 274 )
                        ->save( $dirsmall . 'image' . $i . '.png' );
                Image::make( $file->getRealPath())->resize( 100, 67 )
                        ->save( $dirthumb . 'image' . $i . '.png' );
                $i++;
            }
        }

        return Redirect::to( 'publicar' )
                                ->with( 'create', $create )
                                ->with( 'categories', $this->categorias() )
                                ->with( 'errorpost', $create->error )
                                ->with( 'product_id', $create->product_id )
                                ->with( 'country', $this->tratarota( $create->country ) );

    }

    function tratarota($country){
        $arrEstados = array(  'AM' => 'Amazonas', 'AC' => 'Acre', 'AL' => 'Alagoas', 'AP' => 'Amapá',
                      'CE' => 'Ceará', 'DF' => 'Distrito federal', 'ES' => 'Espirito santo', 'MA' => 'Maranhão',
                      'PR' => 'Paraná', 'PE' => 'Pernambuco', 'PI' => 'Piauí', 'RN' => 'Rio grande do norte',
                      'RS' => 'Rio grande do sul', 'RO' => 'Rondônia', 'RR' => 'Roraima',
                      'SC' => 'Santa catarina', 'SE' => 'Sergipe', 'TO' => 'Tocantins',
                      'PA' => 'Pará', 'BH' => 'Bahia', 'GO' => 'Goiás', 'MT' => 'Mato grosso',
                      'MS' => 'Mato grosso do sul', 'RJ' => 'Rio de janeiro', 'SP' => 'São paulo',
                      'RS' => 'Rio grande do sul', 'MG' => 'Minas gerais', 'PB' => 'Paraiba',
                    );

        return $this->slug( $arrEstados[$country] );
    }

    function slug( $string ) {
        if( is_string( $string ) ):
                $string = strtolower( trim( utf8_decode( $string ) ) );
                
        $before = 'ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõöøùúûýýþÿRr';
        $after  = 'aaaaaaaceeeeiiiidnoooooouuuuybsaaaaaaaceeeeiiiidnoooooouuuyybyRr';       
        $string = strtr( $string , utf8_decode( $before ) , $after );
                
                $replace = array(
                        '/[^a-z0-9.-]/' => '-',
                        '/\-{2,}/'           => ''
                );
                $string = preg_replace('/\s/', '', preg_replace( array_keys( $replace ) , array_values( $replace ) , $string ) );
        endif;
        return $string;
    }

    public function forms($forms){
       return View::make( 'forms._' . $forms);
    }
}