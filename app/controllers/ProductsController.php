<?php

class ProductsController extends BaseController {

    protected $curl;

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(){

        $tccurl = new TccCurl( self::formatRouter() );
        $responses = $tccurl->request();
        
        if ( Sentry::check()){
            $tccurl = new \TccCurl( 'private/getpost' );
            $tccurl->post( ['seller_id' => Sentry::getIdUser() ] );
            $posts = $tccurl->request();

            return View::make( 'products.index', ['products' => $responses, 
                                    'posts' => $posts,
                                    'route' => self::formatRouter()]);
        }

        return View::make( 'products.index', ['products' => $responses, 'route' => self::formatRouter()]);
    }

    public static function productsRegions( $param ){
        $route = $param['city'];

        $tccurl = new TccCurl('productregion' );
        $tccurl->post( $param );
        $responses = $tccurl->request();
        
        if ( Sentry::check()){
            $tccurl = new TccCurl( 'private/getpost' );
            $tccurl->post( ['seller_id' => Sentry::getIdUser() ] );
            $posts = $tccurl->request();

            return View::make( 'products.index', ['products' => $responses, 
                                                    'posts' => $posts, 
                                                    'route' => $route]);
        }
        return View::make( 'products.index', ['products' => $responses, 'route' => $route]);
    }

    public function productsRegionsFindId( $param ){
          // formata dados produto
        $tccurl = new TccCurl( $param['city'] . '/' . $param['id']);
        $responses = $tccurl->request();
        $responses->created_at  = $this->formatDateToDecryption( $responses->created_at );
        $responses->product->created_at = $this->formatDateToDecryption( $responses->product->created_at );
        
        // busca mensagens
        $tccurl = new TccCurl( 'posts/' . $param['id']);
        $posts = $tccurl->request();
        
        return View::make( 'products.show', ['products' => $responses, 'posts' => $posts]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    { 
        // formata dados produto
        $tccurl = new TccCurl( self::formatRouter() . '/' . $id);
        $responses = $tccurl->request();
        
        $responses->created_at  = $this->formatDateToDecryption( $responses->created_at );

        // busca mensagens
        $tccurl = new TccCurl( 'posts/' . $id);
        $posts = $tccurl->request();
        
        return View::make( 'products.show', ['products' => $responses, 'posts' => $posts,
                         'route' => self::formatRouter() ]);
    }

    public function pesquisa( )
    {
        $tccurl = new TccCurl('pesquisar');
        $tccurl->post( Input::all() );
        $responses = $tccurl->request();
        
        if ( Sentry::check()){
            $tccurl = new \TccCurl( 'private/getpost' );
            $tccurl->post( ['seller_id' => Sentry::getIdUser() ] );
            $posts = $tccurl->request();

            return View::make( 'products.index', ['products' => $responses, 
                                    'posts' => $posts,
                                    'route' => Input::get('route') ]);
        }
        
        return View::make( 'products.index', ['products' => $responses, 'route' => Input::get('route') ]);
    }
}