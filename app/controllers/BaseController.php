<?php

class BaseController extends Controller {

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}

    public function formatRouter(){
        $route = explode('.', Route::currentRouteName());
        return $route[0];
    }

    public function xml2array($xmlObject, $out = array()) {
        foreach ((array) $xmlObject as $index => $node)
            $out[$index] = ( is_object($node) ) ? $this->xml2array($node) : $node;

        return $out;
    }

    public function posts(){
    	$tccurl = new TccCurl( 'private/getpost' );
		$tccurl->post( ['seller_id' => Sentry::getIdUser() ] );
		return $tccurl->request();
    }

    public function formatDateToDecryption($date){
        $date = new Date( $date );
        $dateN = new Date('today');

        $dateProduct    = $date->format('date');
        $dateNow        = $dateN->format('date');
        return $date == $dateN ? 'Hoje' : $date->format('short');
        return $dateNow;
    }
}