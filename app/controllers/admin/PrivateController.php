<?php namespace App\Controllers\Admin;

use App\Models\Page;
use App\Services\Validators\PageValidator;
use Input, Notification, Redirect, Sentry, Str;

class PrivateController extends \BaseController {

	public function index()
	{

		$tccurl = new \TccCurl( 'private/getpost' );
		$tccurl->post( ['seller_id' => Sentry::getIdUser() ] );
		$posts = $tccurl->request();
		
		$tccurl = new \TccCurl( 'private/status' );
		$tccurl->post( ['seller_id' => Sentry::getIdUser() ] );
        $responses = $tccurl->request();

		$tccurl = new \TccCurl( 'posts/contposts' );
		$tccurl->post( ['seller_id' => Sentry::getIdUser() ] );
        $contpost = $tccurl->request();
        
        \Session::put('contpost', $contpost->posts);
		return \View::make('admin.pages.index')
					->with('produtos', $responses)
					->with('posts', $posts);
	}

	public function show($id)
	{
		return \View::make('admin.pages.show')->with('page', Page::find($id));
	}

	public function create()
	{
		return \View::make('admin.pages.create');
	}

	public function store()
	{
		$validation = new PageValidator;

		if ($validation->passes())
		{
			$page = new Page;
			$page->title   = Input::get('title');
			$page->slug    = Str::slug(Input::get('title'));
			$page->body    = Input::get('body');
			$page->user_id = Sentry::getUser()->id;
			$page->save();

			Notification::success('The page was saved.');

			return Redirect::route('admin.pages.edit', $page->id);
		}

		return Redirect::back()->withInput()->withErrors($validation->errors);
	}

	public function edit($id)
	{
		return \View::make('admin.pages.edit')->with('page', Page::find($id));
	}

	public function update($id)
	{
		$validation = new PageValidator;

		if ($validation->passes())
		{
			$page = Page::find($id);
			$page->title   = Input::get('title');
			$page->slug    = Str::slug(Input::get('title'));
			$page->body    = Input::get('body');
			$page->user_id = Sentry::getUser()->id;
			$page->save();

			Notification::success('The page was saved.');

			return Redirect::route('admin.pages.edit', $page->id);
		}

		return Redirect::back()->withInput()->withErrors($validation->errors);
	}

	public function destroy($id)
	{
		$page = Page::find($id);
		//$page->delete();

		Notification::success('The page was deleted.');

		return Redirect::route('admin.pages.index');
	}

}
