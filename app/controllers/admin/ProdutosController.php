<?php namespace App\Controllers\Admin;

use App\Models\Page;
use App\Services\Validators\PageValidator;
use Input, Notification, Redirect, Sentry, Str;

class ProdutosController extends \BaseController {

	public function index()
	{
		$tccurl = new \TccCurl('productuser');
		$tccurl->post( ['seller_id' => Sentry::getIdUser()] );
        $responses = $tccurl->request();
		return \View::make('admin.produtos.index', ['products' => $responses, 'posts' => $this->posts()] );
	}

	public function show($id)
	{
		$publicar  = new \PublicarController;
		$tccurl = new \TccCurl('productedit');
		$tccurl->post( ['seller_id' => Sentry::getIdUser(), 'product_id' => $id ] );
        $responses = $tccurl->request();
		return \View::make('admin.produtos.show', [
						'products' => $responses, 
						'categories' => $this->formatCategories($publicar->categorias()), 
						'posts' => $this->posts()] );
	}

	public function formatCategories($categories){
		$category = array();
		foreach ($this->xml2array($categories) as $key => $value):
			foreach ($value as $newkey => $newvalue):
				$category[$newkey] = $newvalue;
			endforeach;
		endforeach;

		return $category;
	}

	public function create()
	{
            $tccurl = new \TccCurl('advertising');
            $responses = $tccurl->request();
        
            return \View::make( 'admin.produtos.create', ['categories' => $responses]);
	}

	public function destacar($id){
		$tccurl = new \TccCurl( 'private/getpost' );
		$tccurl->post( ['seller_id' => Sentry::getIdUser() ] );
		$posts = $tccurl->request();
		return \View::make( 'admin.produtos.destacar', ['product_id' => $id, 'posts' => $posts] );
	}

	public function contratar($id){
		$tccurl = new \TccCurl('contratar');
		$tccurl->post( [ 'periodo' => $id, 'product_id' => \Session::get('product_id') ] );
        $responses = $tccurl->request();

        return Redirect::back()->with(['contratar' => true]);
	}

	public function atualiza(){
		$tccurl = new \TccCurl('productupdate');
		$tccurl->post( array_merge( ['seller_id' => Sentry::getIdUser()], Input::all()) );
        $responses = $tccurl->request();
        return Redirect::back();
	}

	public function desativar($id){
		$tccurl = new \TccCurl('productdesativar');
		$tccurl->post( [ 'product_id' => $id ] );
        $responses = $tccurl->request();
        
        return Redirect::back();
	}
}