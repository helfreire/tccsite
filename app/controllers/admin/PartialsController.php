<?php namespace App\Controllers\Admin;

use App\Models\Page;
use App\Services\Validators\PageValidator;
use Input, Notification, Redirect, Sentry, Str;

class PartialsController extends \BaseController {

    public function produtos(){
        return \View::make('admin.pages.partials.produtos');//->with('pages', Page::all());
    }
   
    public function notificacoes(){
        return \View::make('admin.pages.partials.notificacoes');//->with('pages', Page::all());
    }

}
