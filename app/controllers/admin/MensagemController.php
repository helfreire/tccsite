<?php namespace App\Controllers\Admin;

use App\Models\Page;
use App\Services\Validators\PageValidator;
use Input, Notification, Redirect, Sentry, Str;

class MensagemController extends \BaseController {

	public function index()
	{
		$tccurl = new \TccCurl( 'private/getpost' );
		$tccurl->post( ['seller_id' => Sentry::getIdUser() ] );
		$posts = $tccurl->request();

        return \View::make('admin.mensagens.index', ['posts' => $posts]);
	}

	public function show($id)
	{
		if( \Session::get( 'contpost') > 0 )
			\Session::put( 'contpost', \Session::get('contpost') - 1 ) ;
		
		$tccurl = new \TccCurl( 'private/getposts' );
		$tccurl->post( [ 'post_id' => $id ] );
		$posts = $tccurl->request();
	
		return \View::make('admin.mensagens.show', ['posts' => $posts]);
	}

	public function todasMensagens($id){
		$tccurl = new \TccCurl( 'private/todasmensagens' );
		$tccurl->post( ['product_id' => $id ] );
		$posts = $tccurl->request();
        return \View::make('admin.mensagens.show', ['posts' => $posts]);
		
	}
}
