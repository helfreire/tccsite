<?php namespace App\Controllers\Admin;

use Auth, BaseController, Form, Input, Redirect, Sentry, View;

class AuthController extends BaseController {

	/**
	 * Display the login page
	 * @return View
	 */
	public function getLogin()
	{
	    if ( Sentry::check()){

	    	$tccurl = new \TccCurl( 'private/getpost' );
			$tccurl->post( ['seller_id' => Sentry::getIdUser() ] );
			$posts = $tccurl->request();
			
			$tccurl = new \TccCurl( 'private/status' );
			$tccurl->post( ['seller_id' => Sentry::getIdUser() ] );
	        $responses = $tccurl->request();

			return \View::make('admin.pages.index')
						->with('produtos', $responses)
						->with('posts', $posts);
		}
	    return View::make('admin.auth.login');
	}

	/**
	 * Login action
	 * @return Redirect
	 */
	public function postLogin()
	{

		//dd ( Input::all() );
		$credentials = array(
			'login'    => Input::get('email'),
			'password' => Input::get('password')
		);
                
		try
		{
			$user = Sentry::authenticate($credentials, false);
			if ($user)
			{
				\Session::put('nameuser', $user['attributes']['first_name'] . ' ' . $user['attributes']['last_name']);
				if(Input::get('method') == 'publicar')
					return Redirect::route('publicar.index');


				return Redirect::route('private.index');
			}
		}
		catch(\Exception $e)
		{
			return Redirect::route('admin.login')->withErrors(array('login' => $e->getMessage()));
		}
	}

	/**
	 * Logout action
	 * @return Redirect
	 */
	public function getLogout()
	{
		Sentry::logout();

		return Redirect::route('brasil.index');
	}

}
