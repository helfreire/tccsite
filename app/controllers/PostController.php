<?php

class PostController extends BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        return View::make('posts.index');
	}

	public function setpostsbuyer(){
		if($this->verifica_texto( Input::get('comment') )){
			$tccurl = new TccCurl('postsbuyer');
			$tccurl->post( array_merge( Input::all(), ['seller_id' => Sentry::getIdUser()] ));
	        $responses = $tccurl->request();
        	
	        return 	Redirect::back()->with( [ 'errorpost' => false ] );        	
	    }
	    return 	Redirect::back()->with( [ 'errorpost' => true ] );
	}

	public function ativar($id){
		$tccurl = new TccCurl('post/ativar/' . $id);
		//$tccurl->post( [ 'id' => $id, 'seller_id' => Sentry::getIdUser()] );
        $responses = $tccurl->request();
        
		$tccurl = new TccCurl( 'private/getpost' );
		$tccurl->post( ['seller_id' => Sentry::getIdUser() ] );
		$posts = $tccurl->request();

		return Redirect::back()->with(['posts' => $posts]);
	}

	public function remover($id){
		$tccurl = new TccCurl('post/remover/' . $id);
		//$tccurl->post( [ 'id' => $id, 'seller_id' => Sentry::getIdUser()] );
        $responses = $tccurl->request();
        
		$tccurl = new TccCurl( 'private/getpost' );
		$tccurl->post( ['seller_id' => Sentry::getIdUser() ] );
		$posts = $tccurl->request();

		return Redirect::back()->with(['posts' => $posts]);
	}

	public function responder(){
		$tccurl = new TccCurl('post/responder');
		$tccurl->post( array_merge( Input::all(), [ 'seller_id' => Sentry::getIdUser()] ));
        $responses = $tccurl->request();
		return 	Redirect::back()->with( [ 'errorpost' => false ] );	
	}

		///////// FUNÇÃO QUE VERIFICA NA STRING
	public function verifica_texto ($string){
		return  true;
    	///////// COLOCAR NESSE ARRAY AS PALAVRAS CHAVES PARA PESQUISA
	    $evitar = array("sexo","droga","puta","prostituta");
	    foreach($evitar as $value){
	        $valor = strripos($string, $value);
	        if($valor !== FALSE){
	            $result = 1;
	        }
	    }
	    if($result != 1){
	        return false;
	    }
	    return true;;
	}
}
