<?php

class ProductsTableSeeder extends Seeder {

    public function run()
    {
    	// Uncomment the below to wipe the table clean before populating
    	// DB::table('products')->delete();

        $products = array(
        	array( "name" => "produto 1", "price" => "100,00", "description" => "Descrição produto 1" ),
        	array( "name" => "produto 2", "price" => "200,00", "description" => "Descrição produto 2" )
        );

        // Uncomment the below to run the seeder
         DB::table('products')->insert($products);
    }

}