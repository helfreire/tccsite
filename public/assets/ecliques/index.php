    <!--
To change this template, choose Tools | Templates
and open the template in the editor.
-->
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="estilo.css">
        <link rel="shortcut icon" href="../favicon.ico">
        {{ HTML::style('assets/ecliques/css/bootstrap.min.css') }}
        <link rel="stylesheet" type="text/css" href="css/default.css" />
        <link rel="stylesheet" type="text/css" href="css/component.css" />
        <link rel="stylesheet" href="font-awesome/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="font-awesome/css/font-awesome.min.css">
        <script src="js/modernizr.custom.js"></script>
        <title></title>
    </head>
    <body>
        <div class="esquerda">
            <div class="cima">
                <img src="logo1.png" >
            </div>
            <br><br>
            <span class="titulo1">Clique, Anuncie,<br> Compartilhe e Venda! </span>
            <BR><BR><BR><BR>
            <select id="estados" class="estados">
                <option value="brasil">Selecione o Estado</option>
                <option value="acre">Acre</option>
                <option value="alagoas">Alagoas</option>
                <option value="amapa">Amapá</option>
                <option value="amazonas">Amazonas</option>
                <option value="bahia">Bahia</option>
                <option value="ceara">Ceará</option>
                <option value="distritofederal">Distrito Federal</option>
                <option value="espiritosanto">Espirito Santo</option>
                <option value="goias">Goias</option>
                <option value="maranhao">Maranhão</option>
                <option value="matogrossosul">Mato Grosso do Sul</option>
                <option value="matogrosso">Mato Grosso</option>
                <option value="minasgerais">Minas Gerais</option>
                <option value="para">Pará</option>
                <option value="paraiba">Paraíba</option>
                <option value="parana">Paraná</option>
                <option value="pernambuco">Pernambuco</option>
                <option value="piaui">Piauí</option>
                <option value="riodejaneiro">Rio de Janeiro</option>
                <option value="riograndenorte">Rio Grande do Norte</option>
                <option value="riograndesul">Rio Grande do Sul</option>
                <option value="rondonia">Rondônia</option>
                <option value="roraima">Roraima</option>
                <option value="santacatarina">Santa Catarina</option>
                <option value="saopaulo">São Paulo</option>
                <option value="sergipe">Sergipe</option>
                <option value="tocantins">Tocantins</option>
            </select>
            <br>
            <input type="button" onclick="buscar();" value="Procurar" style="width: 220px"></input>
        </div>
        <nav class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-right" id="cbp-spmenu-s2">
            <h3>Menu</h3>
            <span id="showRight"><i class="icon-home icon-3x"></i></span>	
            <a href="login">Entrar</a>
        </nav>
    </div>
    <!-- Classie - class helper functions by @desandro https://github.com/desandro/classie -->
    <script src="js/classie.js"></script>
    <script>
        var menuLeft = document.getElementById( 'cbp-spmenu-s1' ),
        menuRight = document.getElementById( 'cbp-spmenu-s2' ),
        menuTop = document.getElementById( 'cbp-spmenu-s3' ),
        menuBottom = document.getElementById( 'cbp-spmenu-s4' ),
        showLeft = document.getElementById( 'showLeft' ),
        body = document.body;
        

			
        showRight.onclick = function() {
            classie.toggle( this, 'active' );
            classie.toggle( menuRight, 'cbp-spmenu-open' );
            disableOther( 'showRight' );
        };
        
        
        function buscar(){
            var estado = document.getElementById("estados").value;
             location.href = estado;
        }
	
    </script>
</body>
</html>